<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OwnerUrlController extends Controller
{
    public function dashboard(){
        $data['sekolah'] = \App\School::get();
        $data['kelas'] = \App\Classes::get();
        return view('owner.dashboard')->with($data);
    }
    public function index(){
        $data['provinsi'] = \App\Province::get();
        $data['kota'] = \App\City::get();
        $data['sekolah'] = \App\School::get();
        $data['kelas'] = \App\Classes::get();
        return view('owner.manage.index')->with($data);
    }
    public function kelas(Request $request,$id){
        $data['sekolah'] = \App\School::get();
        $data['kelas'] = \App\Classes::get();
        $data['idsekolah'] =$id;
        return view('owner.manage.kelas')->with($data);
    }
    public function siswa(Request $request){
        return view('owner.manage.siswa');
    }
}
