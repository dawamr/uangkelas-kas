<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OwnerAPIController extends Controller
{
    public function addProvinsi(Request $request){
        $data = new \App\Province;
        $data->name = $request->provinsi;
        $data->jmlkota = $request->jmlkotakab;
        if($data->save()){
            return redirect()->back()->with(['success2' => 'Data Tersimpan !']);
        }else{
            return redirect()->back()->with(['wrong2' => 'Data Tidak Tersimpan !']);
        }

    }
    public function addKota(Request $request){
        $data = new \App\City;
        $data->name = $request->kota;
        $data->jmlsmp = $request->jmlsmp;
        $data->jmlsma = $request->jmlsma;
        $data->jmlsmk = $request->jmlsmk;
        $data->provinsi_id = $request->provinsi_id;
        if($data->save()){
            return redirect()->back()->with(['success3' => 'Data Tersimpan !']);
        }else{
            return redirect()->back()->with(['wrong3' => 'Data Tidak Tersimpan !']);
        }
    }
    public function addSekolah(Request $request){
        $data = new \App\School;
        $data->nama = $request->sekolah;
        $data->provinsi_id = $request->provinsi_id;
        $data->kota_id = $request->kota_id;
        $data->jenjang = $request->jenjang;
        $data->email = $request->sekolah_email;
        $data->website = $request->sekolah_web;
        $data->alamat = $request->sekolah_alamat;
        if($data->save()){
            return redirect()->back()->with(['success1' => 'Data Tersimpan !']);
        }else{
            return redirect()->back()->with(['wrong1' => 'Data Tidak Tersimpan !']);
        }
    }
}
