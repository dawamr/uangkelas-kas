@extends('layouts.master')
@section('title')
<title>Ubold - Responsive Admin Dashboard Template</title>
@endsection
@section('css')
<!-- DataTables -->
<link href="/ubold/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <style>
    </style>
@endsection
@section('content')

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">

                        <h4 class="page-title">KAS Management</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="/">Home</a>
                            </li>
                            <li>
                                <a href="/owner">Dashboard</a>
                            </li>
                            <li class="active">
                                Management KAS
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-lg-12">
                        <div class="portlet">
                            <div class="portlet-heading bg-primary">
                                <h3 class="portlet-title">
                                    KAS Masuk
                                </h3>
                                <div class="portlet-widgets">
                                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                    <span class="divider"></span>
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#panel-masuk"><i class="ion-minus-round"></i></a>
                                    <span class="divider"></span>
                                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="panel-masuk" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <!-- table  -->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card-box table-responsive">

                                        <?php 
                                        $sumber = 'http://localhost:8001/c/js/data.json';
                                        $konten = file_get_contents($sumber);
                                        $data = json_decode($konten, true);
                                        $no = 1;
                                        ?>
                                               

                                                <table id="datatable-kaskelas"
                                                        class="table dt-responsive nowrap" cellspacing="0"
                                                        width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Kecamatan</th>
                                                        <th>Kelurahan</th>
                                                        <th>Kode Pos</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @for($i = 0; $i< count($data); $i++)
                                                    <tr>
                                                        <td><span class="label label-success">{{$no++}}</span></td>
                                                        <td>{{$data[$i]['kecamatan']}}</td>
                                                        <td>{{$data[$i]['kelurahan']}}</td>
                                                        <td>{{$data[$i]['kodepos']}}</td>
                                                    </tr>
                                                    @endfor
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- endtable -->
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                            
                @include('bendahara.modal-kas')

                </div>
            </div> <!-- container -->

        </div> <!-- content -->

@endsection

@section('js')
<script src="/ubold/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/ubold/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="/ubold/assets/plugins/datatables/jszip.min.js"></script>
<script src="/ubold/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/ubold/assets/plugins/datatables/responsive.bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable-kaskelas').dataTable();
        $('#datatable-responsive').DataTable();
        $('#table-kaskeluar').DataTable();
        $('#table-kasmasuk').DataTable();
        $('#table-detkasmasuk').DataTable();
        $('#table-detkaskeluar').DataTable();
        
    });
</script>
<script src="/ubold/assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
@endsection