<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
		<meta name="author" content="Coderthemes">

		<link rel="shortcut icon" href="/ubold/assets/images/favicon_1.ico">

		@yield('title')

		<link href="/ubold/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/ubold/assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="/ubold/assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="/ubold/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="/ubold/assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="/ubold/assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="/ubold/assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" />

        <style>
            a{
                color: #00c4f1;
            }
            .download{
                color:white;
            }
            .download:hover{
                color: white !important;
            }
        </style>
        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        @yield('css')
        <style>
        table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr>th:first-child:before{
            content: '~';
            background-color: #00c4f1 !important;
        }
        table{
            font-size : small;
        }
        </style>
        <script src="/ubold/assets/js/modernizr.min.js"></script>

	</head>

	<body class="fixed-left">

		<!-- Begin page -->
		<div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <a href="index.html" class="logo"><i class="icon-magnet icon-c-logo"></i><span>Ub<i class="md md-album"></i>ld</span></a>
                        <!-- Image Logo here -->
                        <!--<a href="index.html" class="logo">-->
                            <!--<i class="icon-c-logo"> <img src="/ubold/assets/images/logo_sm.png" height="42"/> </i>-->
                            <!--<span><img src="/ubold/assets/images/logo_light.png" height="20"/></span>-->
                        <!--</a>-->
                    </div>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default bg-inverse " role="navigation">
                    <div class="container">
                        <div class="">
                            <div class="pull-left">
                                <button class="button-menu-mobile open-left waves-effect waves-light">
                                    <i class="md md-menu"></i>
                                </button>
                                <span class="clearfix"></span>
                            </div>

                            <ul class="nav navbar-nav hidden-xs">
                                <li><a href="#" class="waves-effect waves-light">Files</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown"
                                       role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span
                                            class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </li>
                            </ul>

                            <form role="search" class="navbar-left app-search pull-left hidden-xs">
			                     <input type="text" placeholder="Search..." class="form-control">
			                     <a href=""><i class="fa fa-search"></i></a>
			                </form>


                            <ul class="nav navbar-nav navbar-right pull-right">
                                <li class="hidden-xs">
                                    <br>
                                    <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>
                                </li>
                                <li class="dropdown top-menu-item-xs">
                                    <br>
                                    <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true"><img src="/ubold/assets/images/users/avatar-1.jpg" alt="user-img" class="img-circle"> </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="javascript:void(0)"><i class="ti-user m-r-10 text-custom"></i> Profile</a></li>
                                        <li><a href="javascript:void(0)"><i class="ti-settings m-r-10 text-custom"></i> Settings</a></li>
                                        <li><a href="javascript:void(0)"><i class="ti-lock m-r-10 text-custom"></i> Lock screen</a></li>
                                        <li class="divider"></li>
                                        <li><a href="javascript:void(0)"><i class="ti-power-off m-r-10 text-danger"></i> Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->


             <!-- Right Sidebar -->
             <div class="left side-menu">
                    <div class="sidebar-inner slimscrollleft">
                        <!--- Divider -->
                        <div id="sidebar-menu">
                            <ul>
            
                                <li class="text-muted menu-title">Navigation</li>
            
                                <li class="has_sub">
                                    <a href="/owner" class="waves-effect"><i class="ti-home"></i> <span> Dashboard </span></a>
                                    <!-- <ul class="list-unstyled">
                                        <li><a href="index.html">Dashboard 1</a></li>
                                        <li><a href="dashboard_2.html">Dashboard 2</a></li>
                                        <li><a href="dashboard_3.html">Dashboard 3</a></li>
                                        <li><a href="dashboard_4.html">Dashboard 4</a></li>
                                    </ul> -->
                                </li>
            
                                <li class="has_sub">
                                    <a href="/owner/manage" class="waves-effect"><i class="ti-paint-bucket"></i> <span> Manage Sekolah </span> </a>
                                    <!-- <ul class="list-unstyled">
                                        <li><a href="ui-buttons.html">Buttons</a></li>
                                        <li><a href="ui-loading-buttons.html">Loading Buttons</a></li>
                                        <li><a href="ui-panels.html">Panels</a></li>
                                        <li><a href="ui-portlets.html">Portlets</a></li>
                                        <li><a href="ui-checkbox-radio.html">Checkboxs-Radios</a></li>
                                        <li><a href="ui-tabs.html">Tabs</a></li>
                                        <li><a href="ui-modals.html">Modals</a></li>
                                        <li><a href="ui-progressbars.html">Progress Bars</a></li>
                                        <li><a href="ui-notification.html">Notification</a></li>
                                        <li><a href="ui-images.html">Images</a></li>
                                        <li><a href="ui-carousel.html">Carousel</a>
                                        <li><a href="ui-video.html">Video</a>
                                        <li><a href="ui-bootstrap.html">Bootstrap UI</a></li>
                                        <li><a href="ui-typography.html">Typography</a></li>
                                    </ul> -->
                                </li>
            
                                <li class="has_sub">
                                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-light-bulb"></i><span> Transaksi </span> <span class="menu-arrow"></span> </a>
                                    <ul class="list-unstyled">
                                        <li><a href="">Terbaru</a></li>
                                        <li><a href="">Semua Transaksi</a></li>
                                    </ul>
                                </li>
            
                                <li class="has_sub">
                                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-spray"></i> <span> Kelola </span> <span class="menu-arrow"></span> </a>
                                    <ul class="list-unstyled">
                                    <li><a style="cursor: pointer;" data-toggle="modal" data-target="#modal-kelas">Kelas</a></li>
                                        <li><a href="icons-materialdesign.html">Pendaftaran</a></li>
                                        <li><a href="icons-ionicons.html">Aktivasi</a></li>
                                        <!-- <li><a href="icons-fontawesome.html">Font awesome</a></li>
                                        <li><a href="icons-themifyicon.html">Themify Icons</a></li>
                                        <li><a href="icons-simple-line.html">Simple line Icons</a></li>
                                        <li><a href="icons-weather.html">Weather Icons</a></li>
                                        <li><a href="icons-typicons.html">Typicons</a></li> -->
                                    </ul>
                                </li>
            
            
                                <li class="text-muted menu-title">More</li>
            
                                <li class="has_sub">
                                    <a href="javascript:void(0);" ><i class="ti-files"></i><span> Detail Akun </span> </a>
                                </li>
            
                                <li class="has_sub">
                                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-gift"></i><span> Pengaturan </span> <span class="menu-arrow"></span></a>
                                    <ul class="list-unstyled">
                                        <li><a href="extra-profile.html">Aplikasi</a></li>
                                        <li><a href="extra-timeline.html">Maintance</a></li>
                                    </ul>
                                </li>
            
                                <li class="has_sub">
                                    <a href="javascript:void(0);"><i class="ti-gift"></i><span class="label label-success pull-right">3</span><span> Keluar </span></a>
                                </li>
            
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            <!-- /Right-bar -->

            @yield('content')   
            @include('owner.owner-modal')
            <footer class="footer">
            © 2016. All rights reserved.
            </footer>

    </div>
        <!-- END wrapper -->

        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="/ubold/assets/js/jquery.min.js"></script>
        <script src="/ubold/assets/js/bootstrap.min.js"></script>
        <script src="/ubold/assets/js/detect.js"></script>
        <script src="/ubold/assets/js/fastclick.js"></script>
        <script src="/ubold/assets/js/jquery.slimscroll.js"></script>
        <script src="/ubold/assets/js/jquery.blockUI.js"></script>
        <script src="/ubold/assets/js/waves.js"></script>
        <script src="/ubold/assets/js/wow.min.js"></script>
        <script src="/ubold/assets/js/jquery.nicescroll.js"></script>
        <script src="/ubold/assets/js/jquery.scrollTo.min.js"></script>


        @yield('js')
        
        <!-- Sweet-Alert  -->
        <script src="/ubold/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
        <script src="/ubold/assets/pages/jquery.sweet-alert.init.js"></script>
        <script src="/ubold/assets/plugins/switchery/js/switchery.min.js"></script>

        <script src="/ubold/assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="/ubold/assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="/ubold/assets/plugins/datatables/jszip.min.js"></script>
        <script src="/ubold/assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="/ubold/assets/plugins/datatables/responsive.bootstrap.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#table-modalkelas').dataTable();
            });
        </script>
        <script src="/ubold/assets/js/jquery.core.js"></script>
        <script src="/ubold/assets/js/jquery.app.js"></script>
	</body>
</html>