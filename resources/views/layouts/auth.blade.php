<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="/ubold/assets/images/favicon_1.ico">

        @yield('title')

        <!--Form Wizard-->
        <link rel="stylesheet" type="text/css" href="/ubold/assets/plugins/jquery.steps/css/jquery.steps.css" />

        <link href="/ubold/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/ubold/assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="/ubold/assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="/ubold/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="/ubold/assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="/ubold/assets/css/responsive.css" rel="stylesheet" type="text/css" />
        
        @yield('style')
        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="/ubold/assets/js/modernizr.min.js"></script>
        
    </head>
    <body>

          @yield('content')

    	<script>
            var resizefunc = [];
        </script>
        @yield('script')
        <!-- jQuery  -->
        <script src="/ubold/assets/js/jquery.min.js"></script>
        <script src="/ubold/assets/js/bootstrap.min.js"></script>
        <script src="/ubold/assets/js/detect.js"></script>
        <script src="/ubold/assets/js/fastclick.js"></script>
        <script src="/ubold/assets/js/jquery.slimscroll.js"></script>
        <script src="/ubold/assets/js/jquery.blockUI.js"></script>
        <script src="/ubold/assets/js/waves.js"></script>
        <script src="/ubold/assets/js/wow.min.js"></script>
        <script src="/ubold/assets/js/jquery.nicescroll.js"></script>
        <script src="/ubold/assets/js/jquery.scrollTo.min.js"></script>


        <script src="/ubold/assets/js/jquery.core.js"></script>
        <script src="/ubold/assets/js/jquery.app.js"></script>

        <!--Form Wizard-->
        <script src="/ubold/assets/plugins/jquery.steps/js/jquery.steps.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="/ubold/assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>

        <!--wizard initialization-->
        <script src="/ubold/assets/pages/jquery.wizard-init.js" type="text/javascript"></script>
	
	</body>
</html> 