@extends('layouts.master')
@section('title')
<title>Ubold - Responsive Admin Dashboard Template</title>
@endsection
@section('css')
<!-- DataTables -->
<link href="/ubold/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>
@endsection
@section('content')

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">

                        <h4 class="page-title">KAS Management</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="/">Home</a>
                            </li>
                            <li>
                                <a href="/owner">Dashboard</a>
                            </li>
                            <li class="active">
                                Management KAS
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-lg-12">
                        <div class="portlet">
                            <div class="portlet-heading bg-danger">
                                <h3 class="portlet-title">
                                    Laporan Bulan NowMounth
                                </h3>
                                <div class="portlet-widgets">
                                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                    <span class="divider"></span>
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#panel-masuk"><i class="ion-minus-round"></i></a>
                                    <span class="divider"></span>
                                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="panel-masuk" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <!-- table  -->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card-box table-responsive">

                                                <h4 class="m-t-0 header-title">
                                                    
                                                </h4>
                                               

                                                <table id="datatable-laporankas"
                                                        class="table dt-responsive nowrap" cellspacing="0"
                                                        width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Nama Siswa</th>
                                                        <th>Total Kas Masuk</th>
                                                        <th>Total Kas Keluar</th>
                                                        <th class="text-center">Aksi</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>Dawam Raja</td>
                                                        <td>Rp. 70.000 ,-</td>
                                                        <td>Rp. 20.000 ,-</td>
                                                        <td class="text-center">
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-sm btn-primary waves-effect"  data-toggle="modal" data-target="#modal-detkaslaporan"><i class="md-visibility"></i> Detail</button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- endtable -->
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                            
                
                <div id="modal-detkaslaporan" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-lg"> 
                        <div class="modal-content"> 
                            <div class="modal-header"> 
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                <h4 class="modal-title">Detail Laporan Kas</h4> 
                            </div> 
                            <div class="modal-body">                                             
                                <!-- table  -->
                                <div class="row">
                                    
                                    <div class="col-md-12"> 
                                        <ul class="nav nav-tabs tabs">
                                            <li class="active tab">
                                                <a href="#tab-kasmasuk" data-toggle="tab" aria-expanded="false"> 
                                                    <span class="visible-xs"><i class="fa fa-home"></i></span> 
                                                    <span class="hidden-xs">KAS Masuk</span> 
                                                </a> 
                                            </li> 
                                            <li class="tab"> 
                                                <a href="#tab-kaskeluar" data-toggle="tab" aria-expanded="false"> 
                                                    <span class="visible-xs"><i class="fa fa-user"></i></span> 
                                                    <span class="hidden-xs">KAS Keluar</span> 
                                                </a> 
                                            </li> 
                                        </ul> 
                                        <div class="tab-content"> 
                                            <div class="tab-pane active" id="tab-kasmasuk"> 
                                                <!-- table  -->
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="card-box table-responsive">

                                                            <table id="table-detlaporankasmasuk"
                                                                    class="table dt-responsive nowrap" cellspacing="0"
                                                                    width="100%">
                                                                <thead>
                                                                <tr>
                                                                    <th>Kas Masuk</th>
                                                                    <th>Tanggal</th>
                                                                    <th>Aksi</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td>Rp. 20.000 ,-</td>
                                                                    <td>Senin, Agustus 2018</td>
                                                                    <td>
                                                                        <form action="">
                                                                            <button class="btn btn-danger btn-sm"><i class="md-delete"></i> Hapus</button>
                                                                        </form>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- endtable -->
                                            </div> 
                                            <div class="tab-pane" id="tab-kaskeluar">
                                                <!-- table  -->
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="card-box table-responsive">

                                                            <table id="table-detlaporankaskeluar"
                                                                    class="table dt-responsive nowrap" cellspacing="0"
                                                                    width="100%">
                                                                <thead>
                                                                <tr>
                                                                    <th>Kas Keluar</th>
                                                                    <th>Keterangan</th>
                                                                    <th>Tanggal</th>
                                                                    <th>Aksi</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td>Rp. 10.000,-</td>
                                                                    <td>Ongkos Pulang</td>
                                                                    <td>Selasa, 18 Agustus 2018</td>
                                                                    <td>
                                                                        <form action="">
                                                                            <button class="btn btn-danger btn-sm"><i class="md-delete"></i> Hapus</button>
                                                                        </form>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- endtable -->
                                            </div> 
                                        </div> 
                                    </div> 

                                </div>
                                <!-- endtable -->
                            </div> 
                            <div class="modal-footer"> 
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                            </div> 
                        </div> 
                    </div>
                </div><!-- /.modal -->


                </div>
            </div> <!-- container -->

        </div> <!-- content -->

@endsection

@section('js')
<script src="/ubold/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/ubold/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="/ubold/assets/plugins/datatables/jszip.min.js"></script>
<script src="/ubold/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/ubold/assets/plugins/datatables/responsive.bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable-laporankas').dataTable();
        $('#table-detlaporankasmasuk').DataTable();
        $('#table-detlaporankaskeluar').DataTable();
        $('#table-kasmasuk').DataTable();
        $('#table-detkasmasuk').DataTable();
        
    });
</script>
<script src="/ubold/assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
@endsection