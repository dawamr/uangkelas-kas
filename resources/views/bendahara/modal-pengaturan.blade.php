<div class="row">
    <div class="col-md-12">
        <div id="modal-setprofile" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog"> 
                <div class="modal-content"> 
                    <div class="modal-header"> 
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                        <h4 class="modal-title">Profile Dawam Raja</h4> 
                    </div> 
                    <div class="modal-body">                                             
                        <!-- table  -->
                        <div class="row">
                            
                            <div class="col-md-12">
                                <div class="profile-detail card-box">
                                    <img src="/ubold/assets/images/users/avatar-2.jpg" class="img-circle" alt="profile-image">

                                    <hr>
                                    <h4 class="text-uppercase font-600">About Dawam</h4>

                                    <form class="form">
                                        <table class="table" style="font-size : medium !important;">
                                            <tr>
                                                <td width="40%" class="font-weight-bold">Nama</td>
                                                <td><input type="text" class="form-control input-sm"></td>
                                                <td><input type="text" class="form-control input-sm"></td>
                                            </tr>
                                            <tr>
                                                <td width="40%" class="font-weight-bold">WhatsApp</td>
                                                <td colspan="2"><input type="text" class="form-control input-sm"></td>
                                            </tr>
                                            <tr>
                                                <td width="40%" class="font-weight-bold">Address</td>
                                                <td colspan="2"><input type="text" class="form-control input-sm"></td>
                                            </tr>
                                            <tr>
                                                <td width="40%" class="font-weight-bold">Email</td>
                                                <td colspan="2"><input type="email" class="form-control input-sm"></td>
                                            </tr>
                                            <tr>
                                                <td width="40%" class="font-weight-bold">Password</td>
                                                <td colspan="2"><input type="password" class="form-control input-sm"></td>
                                            </tr>
                                            <tr>
                                                <td width="40%" class="font-weight-bold">Re-Type Password</td>
                                                <td colspan="2"><input type="password" class="form-control input-sm"></td>
                                            </tr>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        
                        </div>
                        <!-- endtable -->
                    </div> 
                    <div class="modal-footer"> 
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                        <button type="button" class="btn btn-info waves-effect waves-light">Save changes</button> 
                    </div> 
                </div> 
            </div>
        </div><!-- /.modal -->

        <div id="modal-setapp" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog"> 
                <div class="modal-content"> 
                    <div class="modal-header"> 
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                        <h4 class="modal-title">Pengaturan Aplikasi</h4> 
                    </div> 
                    <div class="modal-body">                                             
                        <!-- table  -->
                        <div class="row">
                            
                            <div class="col-md-12"> 
                                <ul class="nav nav-tabs tabs">
                                    <li class="active tab">
                                        <a href="#tab-setkas" data-toggle="tab" aria-expanded="false"> 
                                            <span class="visible-xs"><i class="fa fa-home"></i></span> 
                                            <span class="hidden-xs">Pengaturan Kas</span> 
                                        </a> 
                                    </li> 
                                    <li class="tab"> 
                                        <a href="#tab-setkelas" data-toggle="tab" aria-expanded="false"> 
                                            <span class="visible-xs"><i class="fa fa-user"></i></span> 
                                            <span class="hidden-xs">Pengaturan Kelas</span> 
                                        </a> 
                                    </li> 
                                </ul> 
                                <div class="tab-content"> 
                                    <div class="tab-pane active" id="tab-setkas"> 
                                        <!-- table  -->
                                        <div class="row">
                                            <div class="card-box col-sm-12">
                                                <form action="" class="form">
                                                    <table class="table">
                                                        <tr>
                                                            <td>Nominal Kas Update</td>
                                                            <td>
                                                            <select class="selectpicker" data-style="btn-danger btn-custom">
                                                                <option>Rp. 1.000 ,-</option>
                                                                <option>Rp. 2.000 ,-</option>
                                                                <option>Rp. 3.000 ,-</option>
                                                                <option>Rp. 5.000 ,-</option>
                                                                <option>Rp. 10.000 ,-</option>
                                                                <option>Rp. 25.000 ,-</option>
                                                                <option>Rp. 50.000 ,-</option>
                                                            </select>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- endtable -->
                                    </div> 
                                    <div class="tab-pane" id="tab-setkelas">
                                        <!-- table  -->
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card-box table-responsive">

                                                    <table id="table-detkaskeluar"
                                                            class="table dt-responsive nowrap" cellspacing="0"
                                                            width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th>Kas Keluar</th>
                                                            <th>Keterangan</th>
                                                            <th>Tanggal</th>
                                                            <th>Aksi</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>Rp. 10.000,-</td>
                                                            <td>Ongkos Pulang</td>
                                                            <td>Selasa, 18 Agustus 2018</td>
                                                            <td>
                                                                <form action="">
                                                                    <button class="btn btn-danger btn-sm"><i class="md-delete"></i> Hapus</button>
                                                                </form>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- endtable -->
                                    </div> 
                                </div> 
                            </div> 

                        </div>
                        <!-- endtable -->
                    </div> 
                    <div class="modal-footer"> 
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                    </div> 
                </div> 
            </div>
        </div><!-- /.modal -->

    </div>
</div>