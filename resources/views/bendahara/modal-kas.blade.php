                    <!-- MODAL -->
                    <div class="row">
                        <div class="col-md-12">
                        
                            <div id="modal-kasmasuk" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog"> 
                                    <div class="modal-content"> 
                                        <div class="modal-header"> 
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                            <h4 class="modal-title">KAS Masuk</h4> 
                                        </div> 
                                        <div class="modal-body">                                             
                                            <!-- table  -->
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card-box table-responsive">

                                                        <table id="table-kasmasuk"
                                                                class="table dt-responsive nowrap" cellspacing="0"
                                                                width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th>Nama Siswa</th>
                                                                <th>Kas Masuk</th>
                                                                <th>Status</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td><b>Dawam Raja</b></td>
                                                                <td><input class="form-control" type="number" min="1000"></td>
                                                                <td><span class="label label-success">Lunas</span></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- endtable -->
                                        </div> 
                                        <div class="modal-footer"> 
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                                            <button type="button" class="btn btn-info waves-effect waves-light">Save changes</button> 
                                        </div> 
                                    </div> 
                                </div>
                            </div><!-- /.modal -->

                            <div id="modal-kaskeluar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog"> 
                                    <div class="modal-content"> 
                                        <div class="modal-header"> 
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                            <h4 class="modal-title">KAS Keluar</h4> 
                                        </div> 
                                        <div class="modal-body">                                             
                                            <!-- table  -->
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card-box table-responsive">

                                                        <table id="table-kaskeluar"
                                                                class="table dt-responsive nowrap" cellspacing="0"
                                                                width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th>Nama Siswa</th>
                                                                <th>Kas Keluar</th>
                                                                <th>Keterangan</th>
                                                                <th>Status</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td><b>Dawam Raja</b></td>
                                                                <td><input class="form-control input-sm" type="number" min="1000"></td>
                                                                <td><input class="form-control input-sm" type="text"></td>
                                                                <td><span class="label label-success">Lunas</span></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- endtable -->
                                        </div> 
                                        <div class="modal-footer"> 
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                                            <button type="button" class="btn btn-info waves-effect waves-light">Save changes</button> 
                                        </div> 
                                    </div> 
                                </div>
                            </div><!-- /.modal -->

                            <div id="modal-detailkas" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-lg"> 
                                    <div class="modal-content"> 
                                        <div class="modal-header"> 
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                            <h4 class="modal-title">Detail KAS</h4> 
                                        </div> 
                                        <div class="modal-body">                                             
                                            <!-- table  -->
                                            <div class="row">
                                                
                                                <div class="col-md-12"> 
                                                    <ul class="nav nav-tabs tabs">
                                                        <li class="active tab">
                                                            <a href="#tab-kasmasuk" data-toggle="tab" aria-expanded="false"> 
                                                                <span class="visible-xs"><i class="fa fa-home"></i></span> 
                                                                <span class="hidden-xs">KAS Masuk</span> 
                                                            </a> 
                                                        </li> 
                                                        <li class="tab"> 
                                                            <a href="#tab-kaskeluar" data-toggle="tab" aria-expanded="false"> 
                                                                <span class="visible-xs"><i class="fa fa-user"></i></span> 
                                                                <span class="hidden-xs">KAS Keluar</span> 
                                                            </a> 
                                                        </li> 
                                                    </ul> 
                                                    <div class="tab-content"> 
                                                        <div class="tab-pane active" id="tab-kasmasuk"> 
                                                            <!-- table  -->
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="card-box table-responsive">

                                                                        <table id="table-detkasmasuk"
                                                                                class="table dt-responsive nowrap" cellspacing="0"
                                                                                width="100%">
                                                                            <thead>
                                                                            <tr>
                                                                                <th>Kas Masuk</th>
                                                                                <th>Tanggal</th>
                                                                                <th>Aksi</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <tr>
                                                                                <td>Rp. 20.000 ,-</td>
                                                                                <td>Senin, Agustus 2018</td>
                                                                                <td>
                                                                                    <form action="">
                                                                                        <button class="btn btn-danger btn-sm"><i class="md-delete"></i> Hapus</button>
                                                                                    </form>
                                                                                </td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- endtable -->
                                                        </div> 
                                                        <div class="tab-pane" id="tab-kaskeluar">
                                                            <!-- table  -->
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="card-box table-responsive">

                                                                        <table id="table-detkaskeluar"
                                                                                class="table dt-responsive nowrap" cellspacing="0"
                                                                                width="100%">
                                                                            <thead>
                                                                            <tr>
                                                                                <th>Kas Keluar</th>
                                                                                <th>Keterangan</th>
                                                                                <th>Tanggal</th>
                                                                                <th>Aksi</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <tr>
                                                                                <td>Rp. 10.000,-</td>
                                                                                <td>Ongkos Pulang</td>
                                                                                <td>Selasa, 18 Agustus 2018</td>
                                                                                <td>
                                                                                    <form action="">
                                                                                        <button class="btn btn-danger btn-sm"><i class="md-delete"></i> Hapus</button>
                                                                                    </form>
                                                                                </td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- endtable -->
                                                        </div> 
                                                    </div> 
                                                </div> 

                                            </div>
                                            <!-- endtable -->
                                        </div> 
                                        <div class="modal-footer"> 
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                                        </div> 
                                    </div> 
                                </div>
                            </div><!-- /.modal -->

                            <div id="modal-profilesiswa" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog"> 
                                    <div class="modal-content"> 
                                        <div class="modal-header"> 
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                            <h4 class="modal-title">Profile Dawam Raja</h4> 
                                        </div> 
                                        <div class="modal-body">                                             
                                            <!-- table  -->
                                            <div class="row">
                                                
                                                <div class="col-md-12">
                                                    <div class="profile-detail card-box">
                                                        <div>
                                                            <img src="/ubold/assets/images/users/avatar-2.jpg" class="img-circle" alt="profile-image">

                                                            <hr>
                                                            <h4 class="text-uppercase font-600">About Dawam</h4>

                                                            <div class="text-left">
                                                                <p class="text-muted font-13"><strong>Full Name :</strong> <span class="m-l-15">Johnathan Deo</span></p>

                                                                <p class="text-muted font-13"><strong>Mobile :</strong><span class="m-l-15">(123) 123 1234</span></p>

                                                                <p class="text-muted font-13"><strong>Email :</strong> <span class="m-l-15">coderthemes@gmail.com</span></p>

                                                                <p class="text-muted font-13"><strong>Location :</strong> <span class="m-l-15">USA</span></p>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                            </div>
                                            <!-- endtable -->
                                        </div> 
                                        <div class="modal-footer"> 
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                                        </div> 
                                    </div> 
                                </div>
                            </div><!-- /.modal -->

                            <div id="modal-bulan" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-lg"> 
                                    <div class="modal-content"> 
                                        <div class="modal-header"> 
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                            <h4 class="modal-title">Laporan Kas Custom</h4> 
                                        </div> 
                                        <div class="modal-body">                                             
                                            <div class="p-20">
												<form class="form-horizontal">
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Date Range</label>
                                                        <div class="col-lg-8">
                                                            <div id="reportrange" class="pull-right form-control">
                                                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>												
											</div>
                                        </div> 
                                        <div class="modal-footer"> 
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-info waves-effect waves-light">Save changes</button> 
                                        </div> 
                                    </div> 
                                </div>
                            </div><!-- /.modal -->

                        </div>
                    </div>
                    <!-- ENDMODAL -->