@extends('layouts.auth')
@section('title')
<title>UANGKELAS - Register</title>
@endsection
@section('content')
<div class="account-pages"></div>
		<div class="clearfix"></div>
		<div class="wrapper-page" style="width: 700px !important;">
			<div class=" card-box">
				<div class="panel-heading">
					<h3 class="text-center"> Sign Up to <strong class="text-custom">UangKelas</strong> </h3>
				</div>

				<div class="panel-body">
                    <form id="wizard-validation-form" method="POST" action="/signup">
                      <!-- @csrf -->
                        <div>
                            <h3>Step 1</h3>
                            <section>
                                <div class="form-group clearfix">
                                    <label class="col-lg-2 control-label " for="userName2">Email Address</label>
                                    <div class="col-lg-10">
                                        <input class="required email form-control" id="userName2" name="email" type="email">
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="col-lg-2 control-label " for="password2"> Password *</label>
                                    <div class="col-lg-10">
                                        <input id="password2" name="password" type="password" class="required form-control">

                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <label class="col-lg-2 control-label " for="confirm2">Confirm Password *</label>
                                    <div class="col-lg-10">
                                        <input id="confirm2" name="confirm" type="password" class="required form-control">
                                    </div>
                                </div>
                            </section>
                            <h3>Step 2</h3>
                            <section>

                                <div class="form-group clearfix">
                                    <label class="col-lg-2 control-label" for="name2"> First name *</label>
                                    <div class="col-lg-10">
                                        <input id="name2" name="name" type="text" class="required form-control">
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="col-lg-2 control-label " for="surname2"> Last name *</label>
                                    <div class="col-lg-10">
                                        <input id="surname2" name="surname" type="text" class="required form-control">

                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <label class="col-lg-2 control-label " for="email2">Phone *</label>
                                    <div class="col-lg-10">
                                        <input id="email2" name="text" type="text" class="required form-control">
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <label class="col-lg-2 control-label " for="address2">Address </label>
                                    <div class="col-lg-10">
                                        <input id="address2" name="address" type="text" class="form-control">
                                    </div>
                                </div>

                            </section>
                            <h3>Step Final</h3>
                            <section>
                                <div class="form-group clearfix">
                                    <div class="col-lg-12">
                                        <input id="acceptTerms-2" name="acceptTerms" type="checkbox" class="required">
                                        <label for="acceptTerms-2">I agree with the Terms and Conditions.</label>
                                    </div>
                                </div>

                            </section>
                        </div>
                    </form>
                
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 text-center">
					<p>
						Already have account?<a href="page-login.html" class="text-primary m-l-5"><b>Sign In</b></a>
					</p>
				</div>
			</div>

		</div>
@endsection
