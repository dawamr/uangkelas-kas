@extends('layouts.owner')
@section('title')
<title>Ubold - Responsive Admin Dashboard Template</title>
@endsection
@section('css')
<!-- DataTables -->
<link href="/ubold/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>
@endsection
@section('content')

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">

                        <h4 class="page-title">Management</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="/">Home</a>
                            </li>
                            <li>
                                <a href="/owner">Dashboard</a>
                            </li>
                            <li class="active">
                                Management KAS
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-lg-12">
                        <div class="portlet">
                            <div class="portlet-heading bg-info">
                                <h3 class="portlet-title">
                                    Data {{$sekolah->find($idsekolah)->pluck('nama')->first()}}
                                </h3>
                                <div class="portlet-widgets">
                                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                    <span class="divider"></span>
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#bg-danger"><i class="ion-minus-round"></i></a>
                                    <span class="divider"></span>
                                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="bg-danger" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <!-- table  -->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card-box table-responsive">

                                                <h4 class="m-t-0 header-title">
                                                    <button class="btn btn-info waves-effect waves-light" data-toggle="modal" data-target="#modal-tbhkelas"> Tambah Kelas</button>
                                                </h4>
                                               

                                                <table id="datatable-responsive"
                                                        class="table dt-responsive nowrap" cellspacing="0"
                                                        width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Nama Kelas</th>
                                                        <th>Status</th>
                                                        <th>Jurusan</th>
                                                        <th>Tahun Angkatan</th>
                                                        <th>Jumlah Siswa</th>
                                                        <th>Total KAS</th>
                                                        <th>Total Pengeluaran</th>
                                                        <th>Kas Tersedia</th>
                                                        <th>Siswa Loyal</th>
                                                        <th>Bendahara 1</th>
                                                        <th>Bendahara 2</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($kelas as $data)
                                                    @endforeach
                                                    <tr>
                                                        <td>XII RPL 2</td>
                                                        <td><span class="label label-table label-success">Active</td>
                                                        <td>RPL</td>
                                                        <td>2018 / 2019</td>
                                                        <td>33</td>
                                                        <td>Rp. 1.000.000,-</td>
                                                        <td>Rp. 250.000,-</td>
                                                        <td>Rp. 750.000,-</td>
                                                        <td>Dawam Raja</td>
                                                        <td>Mareta</td>
                                                        <td>Sabrina</td>
                                                        <td>
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-sm btn-warning waves-effect"><i class="md-edit"></i> Edit</button>
                                                                <button type="button" class="btn btn-sm btn-primary waves-effect"><i class="md-visibility"></i> Detail</button>
                                                                <button type="button" class="btn btn-sm btn-danger waves-effect"><i class="md-delete"> </i> Hapus</button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- endtable -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="portlet">
                            <div class="portlet-heading bg-info">
                                <h3 class="portlet-title">
                                    Download Data Kelas (.xls/.csv)
                                </h3>
                                <div class="portlet-widgets">
                                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                    <span class="divider"></span>
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#download-data"><i class="ion-minus-round"></i></a>
                                    <span class="divider"></span>
                                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="download-data" class="panel-collapse collapse in">
                                <div class="portlet-body bg-primary">                                          
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h1><a href="" class="btn btn-block btn-lg waves-effect waves-light download"> Download Format</a></h1>      
                                        </div>
                                    </div> <!--EndROW-->
                                </div>
                            </div>
                        </div>
                    </div><!--ENDCART-->
                    
                    <div class="col-lg-6">
                        <div class="portlet">
                            <div class="portlet-heading bg-info">
                                <h3 class="portlet-title">
                                    Import Data Kelas (.xls/.csv)
                                </h3>
                                <div class="portlet-widgets">
                                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                    <span class="divider"></span>
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#export-data"><i class="ion-minus-round"></i></a>
                                    <span class="divider"></span>
                                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="export-data" class="panel-collapse collapse in">
                                <div class="portlet-body">                                       
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form action="">
                                                <br>
                                                <div class="form-group">
                                                    <input type="file" class="filestyle btn-lg btn-info" data-iconname="fa fa-cloud-upload" placeholder="Upload Data.xls">
                                                </div>
                                            </form>
                                        </div>
                                    </div><!-- EndROW -->
                                </div>
                            </div>
                        </div>
                    </div><!--ENDCART-->
                    
                    <!-- MODAL -->
                    <div class="row">
                        <div class="col-md-12">
                            <div id="modal-tbhkelas" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog"> 
                                    <div class="modal-content"> 
                                        <div class="modal-header"> 
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                            <h4 class="modal-title">Tambah Kelas</h4> 
                                        </div> 
                                        <div class="modal-body"> 
                                            <div class="row">
                                                <div class="col-md-6"> 
                                                    <div class="form-group"> 
                                                        <label for="field-2" class="control-label">Nama Kelas</label> 
                                                        <input type="text" class="form-control" id="field-5" placeholder="Sekolah Negeri 9"> 
                                                    </div> 
                                                </div>
                                                <div class="col-md-6"> 
                                                    <div class="form-group"> 
                                                        <label for="field-2" class="control-label">Pilih Jurusan</label> 
                                                        <select class="form-control">
	                                                        <option>RPL</option>
	                                                        <option>TKJ</option>
                                                            <option>MM</option>
                                                            <option>PS</option>
	                                                        <option>CG</option>
	                                                    </select>
                                                    </div> 
                                                </div>
                                                
                                                <div class="col-md-6"> 
                                                    <div class="form-group"> 
                                                        <label for="field-2" class="control-label">Tahun Angkatan</label> 
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <select class="form-control">
                                                                    <option>2017</option>
                                                                    <option>2018</option>
                                                                    <option>2019</option>
                                                                    <option>2020</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xs-6">
                                                            <select class="form-control">
                                                                    <option>2018</option>
                                                                    <option>2019</option>
                                                                    <option>2020</option>
                                                                    <option>2021</option>
                                                                </select>
                                                            </div>
                                                        </div>    
                                                    </div> 
                                                </div> 
                                                <div class="col-md-6"> 
                                                    <div class="form-group"> 
                                                        <label for="field-2" class="control-label">Website Sekolah</label> 
                                                        <input type="url" min="1" class="form-control" id="field-5" placeholder="sekolahku.sch.id"> 
                                                    </div> 
                                                </div> 
                                                <div class="col-md-12">
                                                    <div class="form-group"> 
                                                        <label for="field-2" class="control-label">Alamat Sekolah</label> 
                                                        <input type="text" min="1" class="form-control" id="field-5" placeholder="Jl. Merdeka 1, Pupusan, Serang"> 
                                                    </div> 
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group"> 
                                                        <label for="field-2" class="control-label">Status Kelas</label> 
                                                        <select class="form-control">
                                                            <option class="label label-table label-success">Active</option>
                                                            <option class="label label-table label-danger">Suspend</option>
                                                        </select>
                                                    </div> 
                                                </div>
                                            </div> 
                                        </div> 
                                        <div class="modal-footer"> 
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                                            <button type="button" class="btn btn-info waves-effect waves-light">Save changes</button> 
                                        </div> 
                                    </div> 
                                </div>
                            </div><!-- /.modal -->
                        </div>
                    </div>
                    <!-- ENDMODAL -->
                </div>
            </div> <!-- container -->

        </div> <!-- content -->
@endsection
@section('js')
<script src="/ubold/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/ubold/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="/ubold/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/ubold/assets/plugins/datatables/responsive.bootstrap.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable').dataTable();
        $('#datatable-responsive').DataTable();
        $('#datatable-responsive2').DataTable();
        $('#data-kota').DataTable();
    });
</script>
<script src="/ubold/assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>

@endsection