@extends('layouts.owner')
@section('title')
<title>Ubold - Responsive Admin Dashboard Template</title>
@endsection
@section('css')
<!-- DataTables -->
<link href="/ubold/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>
@endsection
@section('content')

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">

                        <h4 class="page-title">Management</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="/">Home</a>
                            </li>
                            <li>
                                <a href="/owner">Dashboard</a>
                            </li>
                            <li class="active">
                                Management KAS
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-lg-12">
                        <div class="portlet">
                            <div class="portlet-heading bg-info">
                                <h3 class="portlet-title">
                                    Data Sekolah
                                </h3>
                                <div class="portlet-widgets">
                                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                    <span class="divider"></span>
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#data-sekolah"><i class="ion-minus-round"></i></a>
                                    <span class="divider"></span>
                                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="data-sekolah" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <!-- table  -->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card-box table-responsive">
                                                @if($message = Session::get('success1'))
                                                <div class="alert alert-success">
                                                    <button type="button" class="close" data-dismiss="alert" name="button">x</button>
                                                    <strong>{{$message}}</strong>
                                                </div>
                                                <hr>
                                                @endif
                                                @if($message = Session::get('wrong1'))
                                                <div class="alert alert-danger">
                                                    <button type="button" class="close" data-dismiss="alert" name="button">x</button>
                                                    <strong>{{$message}}</strong>
                                                </div>
                                                <hr>
                                                @endif

                                                <h4 class="m-t-0 header-title">
                                                    <button class="btn btn-info waves-effect waves-light" data-toggle="modal" data-target="#modal-tbhsekolah"> Tambah Sekolah</button>
                                                </h4>
                                               

                                                <table id="datatable-responsive"
                                                        class="table dt-responsive nowrap" cellspacing="0"
                                                        width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Nama Sekolah</th>
                                                        <th>Email Sekolah</th>
                                                        <th>Alamat Sekolah</th>
                                                        <th>Website</th>
                                                        <th>Jenjang</th>
                                                        <th>Kota</th>
                                                        <th>Provinsi</th>
                                                        <th>Kelas Terdaftar</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        
                                                        @foreach($sekolah as $data)
                                                        <tr>
                                                            <td>{{$data->nama}}</td>
                                                            <td>{{$data->email}}</td>
                                                            <td>{{$data->alamat}}</td>
                                                            <td><a href="">{{$data->website}}</a></td>
                                                            <td style="text-transform: uppercase;">{{$data->jenjang}}</td>
                                                            <td>Semarang</td>
                                                            <td>Jawa Tengah</td>
                                                            <td><span class="label label-table label-success">{{count($kelas->where('sekolah_id',$data->id))}}</span></td>
                                                            <td>
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-sm btn-warning waves-effect"><i class="md-edit"></i> Edit</button>
                                                                    <button type="button" class="btn btn-sm btn-primary waves-effect" onclick="location.href='/owner/manage/kelas/{{$data->id}}'"><i class="md-visibility"></i> Detail</button>
                                                                    <button type="button" class="btn btn-sm btn-danger waves-effect"><i class="md-delete"> </i> Hapus</button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- endtable -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="portlet">
                            <div class="portlet-heading bg-info">
                                <h3 class="portlet-title">
                                    Data Provinsi
                                </h3>
                                <div class="portlet-widgets">
                                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                    <span class="divider"></span>
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#data-provinsi"><i class="ion-minus-round"></i></a>
                                    <span class="divider"></span>
                                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="data-provinsi" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                @if($message = Session::get('success2'))
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" name="button">x</button>
                                    <strong>{{$message}}</strong>
                                </div>
                                <hr>
                                @endif
                                @if($message = Session::get('wrong2'))
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert" name="button">x</button>
                                    <strong>{{$message}}</strong>
                                </div>
                                <hr>
                                @endif
                                    <!-- table  -->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card-box table-responsive">

                                                <h4 class="m-t-0 header-title">
                                                    <button class="btn btn-info waves-effect waves-light" data-toggle="modal" data-target="#modal-tbhprovinsi"> Tambah Provinsi</button>
                                                </h4>
                                            

                                                <table id="datatable-responsive2"
                                                        class="table dt-responsive nowrap" cellspacing="0"
                                                        width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Nama Provisi</th>
                                                        <th>Jumlah Kota Terdaftar</th>
                                                        <th >Jumlah Sekolah Terdaftar</th>
                                                        <th>Jumlah Kota/Kabupaten</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($provinsi as $data)
                                                    <tr>
                                                        <td><b>{{$data->name}}</b></td>
                                                        <td><span class="label label-table label-success">{{count($kota->where('provinsi_id', $data->id))}}</span></td>
                                                        <td><span class="label label-table label-success">{{count($sekolah->where('provinsi_id', $data->id))}}</span></td>
                                                        <td><span class="label label-table label-success">{{$data->jmlkota}}</span></td>
                                                        <td>
                                                            <a href="" class="btn btn-warning"> Edit</a>
                                                            <button class="btn btn-danger"> Hapus</button>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- endtable -->
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-6">
                        <div class="portlet">
                            <div class="portlet-heading bg-info">
                                <h3 class="portlet-title">
                                    Data Kota & Kabupaten
                                </h3>
                                <div class="portlet-widgets">
                                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                    <span class="divider"></span>
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#data-kota-box"><i class="ion-minus-round"></i></a>
                                    <span class="divider"></span>
                                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="data-kota-box" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <!-- table  -->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card-box table-responsive">
                                                @if($message = Session::get('success3'))
                                                <div class="alert alert-success">
                                                    <button type="button" class="close" data-dismiss="alert" name="button">x</button>
                                                    <strong>{{$message}}</strong>
                                                </div>
                                                <hr>
                                                @endif
                                                @if($message = Session::get('wrong3'))
                                                <div class="alert alert-danger">
                                                    <button type="button" class="close" data-dismiss="alert" name="button">x</button>
                                                    <strong>{{$message}}</strong>
                                                </div>
                                                <hr>
                                                @endif

                                                <h4 class="m-t-0 header-title">
                                                <button class="btn btn-info waves-effect waves-light" data-toggle="modal" data-target="#modal-tbhkota" data-overlaySpeed="100" data-overlayColor="#36404a"> Tambah Kota</button>  
                                                </h4>
                                            

                                                <table id="data-kota"
                                                        class="table dt-responsive nowrap" cellspacing="0"
                                                        width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Nama Kota / Kab</th>
                                                        <th>Jumlah Sekolah Terdaftar</th>
                                                        <th>Jumlah SMA</th>
                                                        <th>Jumlah SMK</th>
                                                        <th>Jumlah SMP</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($kota as $data)
                                                        <tr>
                                                        <td><b>{{$data->name}}</b></td>
                                                        <td><span class="label label-table label-success">{{count($sekolah->where('kota_id', $data->id))}}</span></td>
                                                        <td><span class="label label-table label-success">{{$data->jmlsma}}</span></td>
                                                        <td><span class="label label-table label-success">{{$data->jmlsmk}}</span></td>
                                                        <td><span class="label label-table label-success">{{$data->jmlsmp}}</span></td>
                                                        <td>
                                                            <button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#con-close-modal"> Edit</button>
                                                            <button class="btn btn-danger waves-effect waves-light"> Hapus</button>
                                                        </td>
                                                         </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- endtable -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="portlet">
                            <div class="portlet-heading bg-info">
                                <h3 class="portlet-title">
                                    Download Format Data (.xls / .csv)
                                </h3>
                                <div class="portlet-widgets">
                                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                    <span class="divider"></span>
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#download-data"><i class="ion-minus-round"></i></a>
                                    <span class="divider"></span>
                                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="download-data" class="panel-collapse collapse in">
                                <div class="portlet-body bg-primary">                                          
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h1><button data-toggle="modal" data-target="#modal-download" class="btn btn-block text-white btn-lg waves-effect waves-light bg-primary download"> Download Format</button></h1>
                                        </div>
                                    </div> <!--EndROW-->
                                </div>
                            </div>
                        </div>
                    </div><!--ENDCART-->
                    
                    <div class="col-lg-6">
                        <div class="portlet">
                            <div class="portlet-heading bg-info">
                                <h3 class="portlet-title">
                                    Import Data (.xls/.csv)
                                </h3>
                                <div class="portlet-widgets">
                                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                    <span class="divider"></span>
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#export-data"><i class="ion-minus-round"></i></a>
                                    <span class="divider"></span>
                                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="export-data" class="panel-collapse collapse in">
                                <div class="portlet-body bg-primary">                                       
                                    <div class="row">
                                        <div class="col-md-12">
                                        <h1><button data-toggle="modal" data-target="#modal-import"class="btn btn-block btn-lg waves-effect waves-light text-white bg-primary download"> Import Format</button></h1>
                                        </div>
                                    </div> <!--EndROW-->
                                </div>
                            </div>
                        </div>
                    </div><!--ENDCART-->

                </div>
                            
                    <!-- MODAL -->
                    <div class="row">
                        <div class="col-md-12">
                            <div id="modal-tbhkota" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog"> 
                                    <div class="modal-content"> 
                                        <div class="modal-header"> 
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                            <h4 class="modal-title">Tambah Kota</h4> 
                                        </div> 

                                        <form action="/owner/api/post/addKota" method="post">
                                            {{csrf_field()}}
                                            <div class="modal-body"> 
                                                <div class="row"> 
                                                    <div class="col-md-6"> 
                                                        <div class="form-group"> 
                                                            <label for="field-1" class="control-label">Nama Kota</label> 
                                                            <input type="text" class="form-control" name="kota" id="field-1" placeholder="Semarang"> 
                                                        </div> 
                                                    </div> 
                                                    <div class="col-md-6"> 
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label">Provinsi</label> 
                                                            <select name="provinsi_id" class="form-control">
                                                                @foreach($provinsi as $provinsi_id)
                                                                <option value="{{$provinsi_id->id}}">{{$provinsi_id->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div> 
                                                    </div> 
                                                </div> 
                                                <div class="row"> 
                                                    <div class="col-md-4"> 
                                                        <div class="form-group"> 
                                                            <label for="field-4" class="control-label">Jumlah SMP</label> 
                                                            <input type="number" min="1" name="jmlsmp" class="form-control" id="field-4" placeholder="123"> 
                                                        </div> 
                                                    </div> 
                                                    <div class="col-md-4"> 
                                                        <div class="form-group"> 
                                                            <label for="field-5" class="control-label">Jumlah SMA</label> 
                                                            <input type="number" min="1" name="jmlsma" class="form-control" id="field-5" placeholder="123"> 
                                                        </div> 
                                                    </div> 
                                                    <div class="col-md-4"> 
                                                        <div class="form-group"> 
                                                            <label for="field-6" class="control-label">Jumlah SMK</label> 
                                                            <input type="number" min="1" name="jmlsmk" class="form-control" id="field-6" placeholder="123"> 
                                                        </div> 
                                                    </div> 
                                                </div> 
                                            </div> 
                                            <div class="modal-footer"> 
                                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                                                <button type="submit" class="btn btn-info waves-effect waves-light">Save changes</button> 
                                            </div> 
                                        </form>

                                    </div> 
                                </div>
                            </div><!-- /.modal -->
                            <div id="modal-tbhprovinsi" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog"> 
                                    <div class="modal-content"> 
                                        <div class="modal-header"> 
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                            <h4 class="modal-title">Tambah Provinsi</h4> 
                                        </div> 
                                        <form action="/owner/api/post/addProvinsi" method="post">
                                        {{csrf_field()}}
                                            <div class="modal-body"> 
                                                <div class="row"> 
                                                    <div class="col-md-6"> 
                                                        <div class="form-group"> 
                                                            <label for="field-1" class="control-label">Nama Privinsi</label> 
                                                            <input type="text" class="form-control" id="field-1" name="provinsi" required placeholder="Nama Provinsi"> 
                                                        </div> 
                                                    </div> 
                                                    <div class="col-md-4"> 
                                                        <div class="form-group"> 
                                                            <label for="field-5" class="control-label">Jumlah Kota / Kabupaten</label> 
                                                            <input name="jmlkotakab" type="number" min="1" class="form-control" placeholder="123" id="field-2" > 
                                                        </div> 
                                                    </div> 
                                                </div> 
                                            </div> 
                                            <div class="modal-footer"> 
                                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                                                <button type="submit" class="btn btn-info waves-effect waves-light">Save changes</button> 
                                            </div> 
                                        </form>
                                    </div> 
                                </div>
                            </div><!-- /.modal -->
                            <div id="modal-download" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content p-0 b-0">
                                        <div class="panel panel-color panel-primary">
                                            <div class="panel-heading"> 
                                                <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button> 
                                                <h3 class="panel-title">Download Format Data</h3> 
                                            </div> 
                                            <div class="panel-body"> 
                                                <div class="container-fluid">
                                                        <a href="" class="download"><h2><i class="md-cloud-download"></i> &nbsp Format Provinsi .xls </h2></a>
                                                        <a href="" class="download"><h2><i class="md-cloud-download"></i> &nbsp Format Kota / Kabupaten .xls </h2></a>
                                                        <a href="" class="download"><h2><i class="md-cloud-download"></i> &nbsp Format Sekolah .xls </h2></a>
                                                </div>
                                            </div> 
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                            <div id="modal-import" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content p-0 b-0">
                                        <div class="panel panel-color panel-primary">
                                            <div class="panel-heading"> 
                                                <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button> 
                                                <h3 class="panel-title">Import Format Data</h3> 
                                            </div> 
                                            <div class="panel-body"> 
                                                <div class="container-fluid">
                                                <form class="form" action="">
                                                    <div class="form-group">
                                                       <span style="font-weight:600">Import Data Sekolah :</span> <input type="file" class="filestyle btn-lg btn-info" data-iconname="fa fa-cloud-upload"><hr>
                                                       <span style="font-weight:600">Import Data Kota :</span> <input type="file" class="filestyle btn-lg btn-info" data-iconname="fa fa-cloud-upload"><hr>
                                                       <span style="font-weight:600">Import Data Provinsi :</span> <input type="file" class="filestyle btn-lg btn-info" data-iconname="fa fa-cloud-upload"><     >
                                                       <button type="submit" class="btn btn-primary btn-block btn-lg">Import Data</button>
                                                    </div>
                                                </form>
                                                </div>
                                            </div> 
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                            <div id="modal-tbhsekolah" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog"> 
                                    <div class="modal-content"> 
                                        <div class="modal-header"> 
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                            <h4 class="modal-title">Tambah Sekolah</h4> 
                                        </div> 

                                        <form action="/owner/api/post/addSekolah" method="post">
                                            {{csrf_field()}}
                                            <div class="modal-body"> 
                                                <div class="row">
                                                    <div class="col-md-6"> 
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label">Nama Sekolah</label> 
                                                            <input type="text" name="sekolah" class="form-control" id="field-5" placeholder="Sekolah Negeri 9"> 
                                                        </div> 
                                                    </div>
                                                    <div class="col-md-6"> 
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label">Pilih Provinsi</label> 
                                                            <select name="provinsi_id" class="form-control">
                                                                @foreach($provinsi as $data)
                                                                <option value="{{$data->id}}">{{$data->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div> 
                                                    </div>
                                                    <div class="col-md-6"> 
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label">Pilih Kota</label> 
                                                            <select name="kota_id" class="form-control">
                                                                @foreach($kota as $data)
                                                                <option value="{{$data->id}}">{{$data->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div> 
                                                    </div>
                                                    <div class="col-md-3"> 
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label">Pilih Jenjang</label> 
                                                            <select name="jenjang" class="form-control">
                                                                <option value="smp">SMP</option>
                                                                <option value="sma">SMA</option>
                                                                <option value="smk">SMK</option>
                                                            </select>
                                                        </div> 
                                                    </div>
                                                    
                                                    <div class="col-md-6"> 
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label">Email Sekolah</label> 
                                                            <input type="email" min="1" name="sekolah_email" class="form-control" id="field-5" placeholder="sekolahku@sch.id"> 
                                                        </div> 
                                                    </div> 
                                                    <div class="col-md-6"> 
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label">Website Sekolah</label> 
                                                            <input type="url" min="1" name="sekolah_web" class="form-control" id="field-5" placeholder="sekolahku.sch.id"> 
                                                        </div> 
                                                    </div> 
                                                    <div class="col-md-12">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label">Alamat Sekolah</label> 
                                                            <input type="text" min="1" name="sekolah_alamat" class="form-control" id="field-5" placeholder="Jl. Merdeka 1, Pupusan, Serang"> 
                                                        </div> 
                                                    </div>
                                                </div> 
                                            </div> 
                                            <div class="modal-footer"> 
                                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                                                <button type="submit" class="btn btn-info waves-effect waves-light">Save changes</button> 
                                            </div> 
                                        </form>

                                    </div> 
                                </div>
                            </div><!-- /.modal -->
                        </div>
                    </div>
                    <!-- ENDMODAL -->
                </div>
            </div> <!-- container -->

        </div> <!-- content -->

@endsection

@section('js')
<script src="/ubold/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/ubold/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="/ubold/assets/plugins/datatables/jszip.min.js"></script>
<script src="/ubold/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/ubold/assets/plugins/datatables/responsive.bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable').dataTable();
        $('#datatable-responsive').DataTable();
        $('#datatable-responsive2').DataTable();
        $('#data-kota').DataTable();
    });
</script>
<script src="/ubold/assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
@endsection