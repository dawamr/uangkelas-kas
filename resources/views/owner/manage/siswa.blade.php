@extends('layouts.owner')
@section('title')
<title>Ubold - Responsive Admin Dashboard Template</title>
@endsection
@section('css')
<!-- DataTables -->
<link href="/ubold/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>
    
@endsection
@section('content')

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">

                        <h4 class="page-title">Management</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="/">Home</a>
                            </li>
                            <li>
                                <a href="/owner">Dashboard</a>
                            </li>
                            <li class="active">
                                Management KAS
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="row">                 
                    <div class="col-lg-12">
                        <div class="portlet">
                            <div class="portlet-heading bg-info">
                                <h3 class="portlet-title">
                                    Data Siswa Kelas @namasekolah
                                </h3>
                                <div class="portlet-widgets">
                                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                    <span class="divider"></span>
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#data-siswa"><i class="ion-minus-round"></i></a>
                                    <span class="divider"></span>
                                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="data-siswa" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <!-- table  -->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card-box table-responsive">

                                                <h4 class="m-t-0 header-title">
                                                    <button class="btn btn-info waves-effect waves-light" data-toggle="modal" data-target="#modal-tbhkelas"> Tambah Siswa</button>
                                                </h4>
                                            
                                                <table id="datatable-responsive"
                                                        class="table dt-responsive table-hover nowrap" cellspacing="0"
                                                        width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Nama Siswa</th>
                                                        <th>NIS</th>
                                                        <th>Email</th>
                                                        <th>WhatsApp</th>
                                                        <th>Tempat Lahir</th>
                                                        <th>Tanggal Lahir</th>
                                                        <th>Alamat Tinggal</th>
                                                        <th>Photo URL</th>
                                                        <th>Status</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>Dawam Raja</td>
                                                        <td>7894</td>
                                                        <td>akunyadawam9@gmail.com</td>
                                                        <td>+1 9298 9999</td>
                                                        <td>Semarang</td>
                                                        <td>14 December 2000</td>
                                                        <td>Jl. Bayem v, Kedungmundu Semarang</td>
                                                        <td><a href="http://imageku.jpg">http://imageku.jpg</a></td>
                                                        <td>Siswa</td>
                                                        <td>
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-sm btn-warning waves-effect"><i class="md-edit"></i> Edit</button>
                                                                <button type="button" class="btn btn-sm btn-primary waves-effect"><i class="md-visibility"></i> Detail</button>
                                                                <button type="button" class="btn btn-sm btn-danger waves-effect"><i class="md-delete"> </i> Hapus</button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- endtable -->
                                </div>
                            </div>
                        </div>
                    </div><!--ENDCART-->
                    
                    <div class="col-lg-6">
                        <div class="portlet">
                            <div class="portlet-heading bg-info">
                                <h3 class="portlet-title">
                                    Download Data Siswa (.xls/.csv)
                                </h3>
                                <div class="portlet-widgets">
                                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                    <span class="divider"></span>
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#download-data"><i class="ion-minus-round"></i></a>
                                    <span class="divider"></span>
                                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="download-data" class="panel-collapse collapse in">
                                <div class="portlet-body bg-primary">                                          
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h1><a href="" class="btn btn-block btn-lg waves-effect waves-light download"> Download Format</a></h1>      
                                        </div>
                                    </div> <!--EndROW-->
                                </div>
                            </div>
                        </div>
                    </div><!--ENDCART-->
                    
                    <div class="col-lg-6">
                        <div class="portlet">
                            <div class="portlet-heading bg-info">
                                <h3 class="portlet-title">
                                    Import Data Siswa (.xls/.csv)
                                </h3>
                                <div class="portlet-widgets">
                                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                    <span class="divider"></span>
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#export-data"><i class="ion-minus-round"></i></a>
                                    <span class="divider"></span>
                                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="export-data" class="panel-collapse collapse in">
                                <div class="portlet-body">                                       
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form action="">
                                                <br>
                                                <div class="form-group">
                                                    <input type="file" class="filestyle btn-lg btn-info" data-iconname="fa fa-cloud-upload" placeholder="Upload Data.xls">
                                                </div>
                                            </form>
                                        </div>
                                    </div><!-- EndROW -->
                                </div>
                            </div>
                        </div>
                    </div><!--ENDCART-->
                    
                    <!-- MODAL -->
                    <div class="row">
                        <div class="col-md-12">
                            <div id="modal-tbhkelas" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog"> 
                                    <div class="modal-content"> 
                                        <div class="modal-header"> 
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                            <h4 class="modal-title">Tambah Siswa</h4> 
                                        </div> 
                                        <div class="modal-body"> 
                                            <div class="row">
                                                <div class="col-md-6"> 
                                                    <div class="form-group"> 
                                                        <label for="field-2" class="control-label">Nama Siswa</label> 
                                                        <input type="text" class="form-control" id="field-5" placeholder="Nama Siswa"> 
                                                    </div> 
                                                </div>
                                                <div class="col-md-3"> 
                                                    <div class="form-group"> 
                                                        <label for="field-2" class="control-label">NIS</label> 
                                                        <input type="text" class="form-control" id="field-5" placeholder="1234"> 
                                                    </div> 
                                                </div>
                                                <div class="col-md-6"> 
                                                    <div class="form-group"> 
                                                        <label for="field-2" class="control-label">Email</label> 
                                                        <input type="text" class="form-control" id="field-5" placeholder="siswa@gmail.com"> 
                                                    </div> 
                                                </div>
                                                <div class="col-md-6"> 
                                                    <div class="form-group"> 
                                                        <label for="field-2" class="control-label">WhatsApp</label> 
                                                        <input type="text" class="form-control" id="field-5" placeholder="+62 885xxxxxx"> 
                                                    </div> 
                                                </div>
                                                <div class="col-md-6"> 
                                                    <div class="form-group"> 
                                                        <label for="field-2" class="control-label">Tempat Lahir</label> 
                                                        <input type="text" class="form-control" id="field-5" placeholder="Kota Kelahiran"> 
                                                    </div> 
                                                </div>
                                                <div class="col-md-6"> 
                                                    <div class="form-group"> 
                                                        <label for="field-2" class="control-label">Tanggal Lahir</label> 
                                                        <input type="date" class="form-control" id="field-5"> 
                                                    </div> 
                                                </div>
                                                <div class="col-md-12"> 
                                                    <div class="form-group"> 
                                                        <label for="field-2" class="control-label">Alamat Tinggal</label> 
                                                        <input type="text" class="form-control" id="field-5" placeholder="Alamat tinggal"> 
                                                    </div> 
                                                </div>
                                                <div class="col-md-6"> 
                                                    <div class="form-group"> 
                                                        <label for="field-2" class="control-label">URL Foto Profile</label> 
                                                        <input parsley-type="url" type="url" class="form-control" required="" placeholder="URL"> 
                                                    </div> 
                                                </div>
                                                <div class="col-md-6"> 
                                                    <div class="form-group"> 
                                                        <label for="field-2" class="control-label">Status</label> 
                                                            <select class="form-control">
                                                                <option>Bendahara</option>
                                                                <option>Siswa</option>
                                                                <option>Wali Kelas</option>
                                                        </select>
                                                    </div>
                                                </div> 
                                            </div> 
                                        </div> 
                                        <div class="modal-footer"> 
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                                            <button type="button" class="btn btn-info waves-effect waves-light">Save changes</button> 
                                        </div> 
                                    </div> 
                                </div>
                            </div><!-- /.modal -->
                        </div>
                    </div>
                    <!-- ENDMODAL -->

                </div>
            </div> <!-- container -->

        </div> <!-- content -->

@endsection

@section('js')
<script src="/ubold/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/ubold/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="/ubold/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/ubold/assets/plugins/datatables/responsive.bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable').dataTable();
        $('#datatable-responsive').DataTable();
        $('#datatable-responsive2').DataTable();
        $('#data-kota').DataTable();
    });
    TableManageButtons.init();

</script>
<script src="/ubold/assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>

@endsection