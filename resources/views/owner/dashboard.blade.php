@extends('layouts.owner')
@section('title')
<title>Ubold - Responsive Admin Dashboard Template</title>
@endsection
@section('css')
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="/ubold/assets/plugins/morris/morris.css">
    <link href="/ubold/assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
    
@endsection
@section('content')

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->                      
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="btn-group pull-right m-t-15">
                                    <button type="button" class="btn btn-default dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">Settings <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
                                    <ul class="dropdown-menu drop-menu-right" role="menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>
                                <h4 class="page-title">Dashboard 2</h4>
                                <p class="text-muted page-title-alt">Welcome to UANGKELAS v2 admin panel !</p>
                            </div>
                        </div>

                        
                        <div class="row">
							<div class="col-lg-4">
								<div class="card-box">
									<div class="bar-widget">
										<div class="table-box">
											<div class="table-detail">
												<div class="iconbox bg-info">
													<i class="icon-layers"></i>
												</div>
											</div>

											<div class="table-detail">
											   <h4 class="m-t-0 m-b-5"><b></b></h4>
											   <h5 class="text-muted m-b-0 m-t-0">SISWA - ALUMNI</h5>
											</div>

										</div>
									</div>
								</div>
							</div>

                            <div class="col-lg-4">
								<div class="card-box">
									<div class="bar-widget">
										<div class="table-box">
											<div class="table-detail">
												<div class="iconbox bg-custom">
													<i class="icon-layers"></i>
												</div>
											</div>

											<div class="table-detail">
											   <h4 class="m-t-0 m-b-5"><b></b></h4>
											   <h5 class="text-muted m-b-0 m-t-0">Perusahaan</h5>
											</div>

										</div>
									</div>
								</div>
							</div>

                            <div class="col-lg-4">
								<div class="card-box">
									<div class="bar-widget">
										<div class="table-box">
											<div class="table-detail">
												<div class="iconbox bg-danger">
													<i class="icon-layers"></i>
												</div>
											</div>

											<div class="table-detail">
											   <h4 class="m-t-0 m-b-5"><b></b></h4>
											   <h5 class="text-muted m-b-0 m-t-0">KELAS - JURUSAN</h5>
											</div>

										</div>
									</div>
								</div>
							</div>
						</div><!-- end row -->

						<div class="row">
                            <!-- Transactions -->
                            <div class="col-lg-4">
                            	<div class="card-box">
									<h4 class="m-t-0 m-b-20 header-title"><b>PENDAFTAR BARU</b><a class="pull-right" href="http://">Lihat Semua</a></h4>

									<div class="nicescroll mx-box">
                                        <ul class="list-unstyled transaction-list m-r-5">

                                        <li>
                                                <i class="ti-user text-primary"></i>
                                                <span class="tran-text">nama</span>
                                                <span class="pull-right text-danger tran-price">Accept</span>
                                                <span class="pull-right text-muted">tgl</span>
                                                <span class="pull-right text-muted">dd &nbsp &nbsp</span>
                                                <span class="pull-right text-muted">mdmd &nbsp &nbsp</span>
                                                <span class="clearfix"></span>
                                            </li>

                                        </ul>
                                    </div>
								</div>

                            </div> <!-- end col -->

                            <!-- CHAT -->
                            <div class="col-lg-4">
                            	<div class="card-box">
                            		<h4 class="m-t-0 m-b-20 header-title"><b>PESAN BARU</b><a class="pull-right" href="http://">Lihat Semua</a></h4>


                            		<div class="chat-conversation">
                                        <ul class="conversation-list nicescroll">
                                            <li class="clearfix">
                                                <div class="chat-avatar">
                                                    <img src="/ubold/assets/images/avatar-1.jpg" alt="male">
                                                    <i>10:00</i>
                                                </div>
                                                <div class="conversation-text">
                                                    <div class="ctext-wrap">
                                                        <i>John Deo</i>
                                                        <p>
                                                            Hello!
                                                        </p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="clearfix">
                                                <div class="chat-avatar">
                                                    <img src="/ubold/assets/images/avatar-1.jpg" alt="male">
                                                    <i>10:01</i>
                                                </div>
                                                <div class="conversation-text">
                                                    <div class="ctext-wrap">
                                                        <i>John Deo</i>
                                                        <p>
                                                            Yeah everything is fine
                                                        </p>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                            	</div>

                            </div> <!-- end col-->


                            <!-- Todos app -->
                            <div class="col-lg-4">
                            	<div class="card-box">
									<h4 class="m-t-0 m-b-20 header-title"><b>Todo</b></h4>
									<div class="todoapp">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <h4 id="todo-message"><span id="todo-remaining"></span> of <span id="todo-total"></span> remaining</h4>
                                            </div>
                                            <div class="col-sm-6">
                                                <a href="" class="pull-right btn btn-primary btn-sm waves-effect waves-light" id="btn-archive">Archive</a>
                                            </div>
                                        </div>

                                        <ul class="list-group no-margn nicescroll todo-list" style="height: 280px" id="todo-list"></ul>

                                         <form name="todo-form" id="todo-form" role="form" class="m-t-20">
                                            <div class="row">
                                                <div class="col-sm-9 todo-inputbar">
                                                    <input type="text" id="todo-input-text" name="todo-input-text" class="form-control" placeholder="Add new todo">
                                                </div>
                                                <div class="col-sm-3 todo-send">
                                                    <button class="btn-primary btn-md btn-block btn waves-effect waves-light" type="button" id="todo-btn-submit">Add</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
								</div>

                            </div> <!-- end col -->

                            <!-- CHAT -->
                            <div class="col-lg-4">
                            	<div class="card-box">
                            		<h4 class="m-t-0 m-b-20 header-title"><b>TULIS PESAN</b></h4>

                            		<div class="chat-conversation">
                                        <div class="chat-conversation">
                                            <ul class="conversation-list nicescroll">
                                                <li class="clearfix">
                                                <input type="email" name="email" placeholder="Email" class="form-control" id="">
                                                </li>
                                                <li class="clearfix">
                                                    <div class="inline-editor">
                                                        <br>
                                                        <p> Ketik Pesan</p>
                                                        <br>
                                                    </div>
                                                </li>
                                                <li class="clearfix">
                                                    <button type="submit" class="btn btn-block btn-primary">Kirim</button>
                                                </li>
                                            </ul>
                                        </div>

                                    </div>
                            	</div>

                            </div> <!-- end col-->

                            <!-- PENGUMUMAN -->
                            <div class="col-lg-4">
                            	<div class="card-box">
                            		<h4 class="m-t-0 m-b-20 header-title"><b>TULIS PENGUMUMAN</b></h4>

                            		<div class="chat-conversation">
                                        <div class="chat-conversation">
                                            <ul class="conversation-list nicescroll">
                                                <li class="clearfix">
                                                    <input type="text" class="typeahead form-control">
                                                </li>
                                                <li class="clearfix">
                                                    <div class="inline-editor2">
                                                        <br>
                                                        <p> Ketik Pesan</p>
                                                        <br>
                                                    </div>
                                                </li>
                                                <li class="clearfix">
                                                    <button type="submit" class="btn btn-block btn-primary">Kirim</button>
                                                </li>
                                            </ul>
                                        </div>

                                    </div>
                            	</div>

                            </div> <!-- end col-->
                        </div> <!-- end row -->

                        
                        

                    </div> <!-- container -->
                               
                </div> <!-- content -->

@endsection
@section('js')

<script src="/ubold/assets/plugins/moment/moment.js"></script>
<script src="/js/typeahead.min.js"></script>
<script type="text/javascript">
    var path = "/admin/api/searchSiswa";
    $('input.typeahead').typeahead({
        source:  function (query, process) {
        return $.get(path, { query: query }, function (data) {
                return process(data);
            });
        }
    });
    var path2 = "/admin/api/searchDudi";
    $('input.typeahead2').typeahead({
        source:  function (query, process) {
        return $.get(path2, { query: query }, function (data) {
                return process(data);
            });
        }
    });
</script>


<script src="/ubold/assets/plugins/raphael/raphael-min.js"></script>

 <script src="/ubold/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>

<!-- Todojs  -->
<script src="/ubold/assets/pages/jquery.todo.js"></script>

<!-- chatjs  -->
<script src="/ubold/assets/pages/jquery.chat.js"></script>

<script src="/ubold/assets/plugins/peity/jquery.peity.min.js"></script>

<!-- <script src="/ubold/assets/pages/jquery.dashboard_2.js"></script> -->

@endsection
