    <!-- MODAL -->
    <div class="row">
        <div class="col-md-12">

            <div id="modal-kelas" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-lg"> 
                    <div class="modal-content"> 
                        <div class="modal-header"> 
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                            <h4 class="modal-title">Kelola Kelas</h4> 
                        </div> 
                        <div class="modal-body">                                             
                            <!-- table  -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-box table-responsive">

                                        <table id="table-modalkelas"
                                                class="table dt-responsive nowrap" cellspacing="0"
                                                width="100%">
                                            <thead>
                                            <tr>
                                                <th>Nama Kelas</th>
                                                <th>Sekolah</th>
                                                <th>Jumlah Siswa</th>
                                                <th>Status</th>
                                                <th>Expired</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($kelas as $data)
                                            <tr>
                                                <td><b><a href="/owner/manage/kelas/{{$data->id}}">{{$data->nama}}</a></b></td>
                                                <td>{{$sekolah->where('id',$data->sekolah_id)->pluck('nama')->first()}}</td>
                                                <td>33</td>
                                                <td><span class="label label-table label-success"> </span></td>
                                                <td>23-11-2002</td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- endtable -->
                        </div> 
                        <div class="modal-footer"> 
                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                        </div> 
                    </div> 
                </div>
            </div><!-- /.modal -->
            
        </div>
    </div>


                        