<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/maintance', function(){
    return view('maintance.maintance');
});
Route::get('/cekPos', function(){
    return view('apiPos.index');
});
Route::get('/cooming-soon', function(){
    return view('maintance.cooming');
});
Route::get('/logout', function(){
    session()->flush();
    return redirect('/');
});

Route::get('/test/1', function(){
    return view('test.test1');
});




Auth::routes();
Route::get('/', 'HomeController@index')->name('home');

// Owner Role
Route::group(['prefix'=>'owner', 'middleware'=>['auth','role:owner']], function () {

    Route::get('/','OwnerUrlController@dashboard');
    Route::get('/manage','OwnerUrlController@index');
    Route::get('/manage/kelas/{id}','OwnerUrlController@kelas');
    Route::get('/manage/siswa','OwnerUrlController@siswa');

    Route::post('/api/post/addProvinsi','OwnerAPIController@addProvinsi');
    Route::post('/api/post/addKota','OwnerAPIController@addKota');
    Route::post('/api/post/addSekolah','OwnerAPIController@addSekolah');

    Route::get('/shell/php-artisan-migrate-fresh', function() {
        Artisan::call('migrate:fresh');
       return redirect()->back();
   });
   Route::get('/shell/php-artisan-migrate-fresh', function() {
        Artisan::call('migrate');
    return redirect()->back();
    });
   Route::get('/shell/php-artisan-db-seed', function() {
        Artisan::call('db:seed');
       return redirect()->back();
   });
   
 
});

// Admin Role
Route::group(['prefix'=>'admin', 'middleware'=>['auth','role:admin']], function () {

});

// Bendahara Role
Route::group(['prefix'=>'bendahara'], function () {
    Route::get('/', function(){
        return view('bendahara.dashboard');
    });
    Route::get('/kas', function(){
        return view('bendahara.kas');
    });
    Route::get('/1/laporan', function(){
        return view('bendahara.laporan.bulanan');
    });
    Route::get('/2/laporan', function(){
        return view('bendahara.laporan.custom');
    });
});

// Guru Role
Route::group(['prefix'=>'guru', 'middleware'=>['auth','role:guru']], function () {

});

// Siswa Role
Route::group(['prefix'=>'siswa', 'middleware'=>['auth','role:siswa']], function () {

});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
