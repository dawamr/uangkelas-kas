<?php

use Illuminate\Database\Seeder;
use App\Angkatan;
use App\User;
use App\Classes;
use App\School;
use App\Role;
class KelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      // Membuat role admin
      $ownerRole = new Role();
      $ownerRole->name = "owner";
      $ownerRole->display_name = "Dawam Raja";
      $ownerRole->description = "";
      $ownerRole->save();

      // Membuat role Pengelola
      $adminRole = new Role();
      $adminRole->name = "admin";
      $adminRole->display_name = "Pengelola Sekolah";
      $adminRole->description = "";
      $adminRole->save();

      // Membuat role Bendahara
      $petugasRole = new Role();
      $petugasRole->name = "petugas";
      $petugasRole->display_name = "Bendahara Kelas";
      $petugasRole->description = "";
      $petugasRole->save();

      // Membuat role Siswa
      $siswaRole = new Role();
      $siswaRole->name = "siswa";
      $siswaRole->display_name = "Siswa";
      $siswaRole->description = "";
      $siswaRole->save();

      // Membuat role Guru
      $guruRole = new Role();
      $guruRole->name = "guru";
      $guruRole->display_name = "Guru";
      $guruRole->description = "";
      $guruRole->save();
      // Membuat role Sekertaris
      // $sekertarisRole = new Role();
      // $sekertarisRole->name = "sekertaris";
      // $sekertarisRole->display_name = "Sekertaris Kelas";
      // $sekertarisRole->description = "";
      // $sekertarisRole->save();

      //
      // $ortuRole = new Role();
      // $ortuRole->name = "Orangtua";
      // $ortuRole->display_name = "Orang Tua";
      // $ortuRole->description = "";
      // $ortuRole->save();

      // Membuat sample admin
      $owner = new User();
      $owner->name = 'Admin Dawam';
      $owner->email = 'dawam@uangkelas.com';
      $owner->password = bcrypt('rahasia');
      $owner->save();
      $owner->attachRole($ownerRole);

      //Membuat Tahun
      $tahun = 2018;
      
      for ($i=1; $i <7 ; $i++) { 
        $data[$i] = new Angkatan();
        $data[$i]->angkatan = ($tahun + $i);
        $data[$i]->kepala = "Boss Luluk";
        $data[$i]->save();
      } 

      for($a=1;$a<2;$a++){
        $provinsi[$a] = new \App\Province();
        $provinsi[$a]->name = "Jawa Tengah";
        $provinsi[$a]->jmlkota = 25;
        $provinsi[$a]->save();
        for($b=1;$b<2;$b++){
          $kota[$b]= new \App\City();
          $kota[$b]->name="Semarang";
          $kota[$b]->jmlsmp= 42;
          $kota[$b]->jmlsma=16;
          $kota[$b]->jmlsmk=11;
          $kota[$b]->provinsi_id= $provinsi[$a]->id;
          $kota[$b]->save();
          for($c=1;$c<2;$c++){
            $sekolah[$c] = new \App\School();
            $sekolah[$c]->nama = "SMK N 8 Semarang";
            $sekolah[$c]->provinsi_id = $provinsi[$a]->id;
            $sekolah[$c]->kota_id = $kota[$b]->id;
            $sekolah[$c]->jenjang = 'smk';
            $sekolah[$c]->email = 'smkn8.smg@gmail.com';
            $sekolah[$c]->website = 'www.smkn8smg.sch.id';
            $sekolah[$c]->alamat = 'Jl. Pandanaran 2';
            $sekolah[$c]->save();
            for($d=1;$d<2;$d++){
              $jurusan[$d] = new \App\Jurusan();
              $jurusan[$d]->nama= "Rekayasa Perangkat Lunak";
              $jurusan[$d]->sekolah_id= $sekolah[$c]->id;
              $jurusan[$d] = new \App\Jurusan();
              $jurusan[$d]->nama= "Teknik Komputer Jaringan";
              $jurusan[$d]->sekolah_id= $sekolah[$c]->id;
              $jurusan[$d]->save();
              $jurusan[$d] = new \App\Jurusan();
              $jurusan[$d]->nama= "Perawatan Sosial";
              $jurusan[$d]->sekolah_id= $sekolah[$c]->id;
              $jurusan[$d]->save();
              $jurusan[$d] = new \App\Jurusan();
              $jurusan[$d]->nama= "Multimedia";
              $jurusan[$d]->sekolah_id= $sekolah[$c]->id;
              $jurusan[$d]->save();
              $jurusan[$d] = new \App\Jurusan();
              $jurusan[$d]->nama= "Care Griver";
              $jurusan[$d]->sekolah_id= $sekolah[$c]->id;
              $jurusan[$d]->save();
              for($e=1; $e<10; $e++){
                $kelas[$e]= new \App\Classes();
                if($e>=1 && $e<4){
                  $kelas[$e]->nama= "X RPL " .$e;
                }
                if($e==4){$kelas[$e]->nama= "XI RPL 1";}
                if($e==5){$kelas[$e]->nama= "XI RPL 2";}
                if($e==6){$kelas[$e]->nama= "XI RPL 3";}
                if($e==7){$kelas[$e]->nama= "XII RPL 1";}
                if($e==8){$kelas[$e]->nama= "XII RPL 2";}
                if($e==9){$kelas[$e]->nama= "XII RPL 3";}
                $kelas[$e]->jurusan_id= $jurusan[$d]->id;
                $kelas[$e]->angkatan_id= 1; 
                $kelas[$e]->sekolah_id=  $sekolah[$c]->id;
                $kelas[$e]->save();
              }
              for($e=1; $e<10; $e++){
                $kelas[$e]= new \App\Classes();
                if($e>=1 && $e<4){
                  $kelas[$e]->nama= "X PS " .$e;
                }
                if($e==4){$kelas[$e]->nama= "XI PS 1";}
                if($e==5){$kelas[$e]->nama= "XI PS 2";}
                if($e==6){$kelas[$e]->nama= "XI PS 3";}
                if($e==7){$kelas[$e]->nama= "XII PS 1";}
                if($e==8){$kelas[$e]->nama= "XII PS 2";}
                if($e==9){$kelas[$e]->nama= "XII PS 3";}
                $kelas[$e]->jurusan_id= $jurusan[$d]->id;
                $kelas[$e]->angkatan_id= 1; 
                $kelas[$e]->sekolah_id=  $sekolah[$c]->id;
                $kelas[$e]->save();
              }
              for($e=1; $e<10; $e++){
                $kelas[$e]= new \App\Classes();
                if($e>=1 && $e<4){
                  $kelas[$e]->nama= "X MM " .$e;
                }
                if($e==4){$kelas[$e]->nama= "XI MM 1";}
                if($e==5){$kelas[$e]->nama= "XI MM 2";}
                if($e==6){$kelas[$e]->nama= "XI MM 3";}
                if($e==7){$kelas[$e]->nama= "XII MM 1";}
                if($e==8){$kelas[$e]->nama= "XII MM 2";}
                if($e==9){$kelas[$e]->nama= "XII MM 3";}
                $kelas[$e]->jurusan_id= $jurusan[$d]->id;
                $kelas[$e]->angkatan_id= 1; 
                $kelas[$e]->sekolah_id=  $sekolah[$c]->id;
                $kelas[$e]->save();
              }
              for($e=1; $e<9; $e++){
                $kelas[$e]= new \App\Classes();
                if($e>=1 && $e<4){
                  $kelas[$e]->nama= "X TKJ " .$e;
                }
                if($e==4){$kelas[$e]->nama= "XI TKJ 1";}
                if($e==5){$kelas[$e]->nama= "XI TKJ 2";}
                if($e==6){$kelas[$e]->nama= "XI TKJ 3";}
                if($e==7){$kelas[$e]->nama= "XII TKJ 1";}
                if($e==8){$kelas[$e]->nama= "XII TKJ 2";}
                $kelas[$e]->jurusan_id= $jurusan[$d]->id;
                $kelas[$e]->angkatan_id= 1; 
                $kelas[$e]->sekolah_id=  $sekolah[$c]->id;
                $kelas[$e]->save();
              }
              for($e=1; $e<2; $e++){
                $kelas[$e]= new \App\Classes();
                $kelas[$e]->nama= "X CG 1";
                $kelas[$e]->jurusan_id= $jurusan[$d]->id;
                $kelas[$e]->angkatan_id= 1; 
                $kelas[$e]->sekolah_id=  $sekolah[$c]->id;
                $kelas[$e]->save();
              }
            }
          }
        }
      }
      

      // for ($a=1 ;$a <11 ; $a++) {
      //   $sekolah[$a] = new \App\Sekolah;
      //   $sekolah[$a]->nama = "SMK Negri " . $a;
      //   $sekolah[$a]->email = "smkn".$a."@gmail.com";
      //   $sekolah[$a]->alamat = "Semarang";
      //   $sekolah[$a]->save();
      //     for ($x=1; $x < 2; $x++) {
      //     $kelas[$x] = new Kelas();
      //     $kelas[$x]->tahun_id = 2;
      //     $kelas[$x]->sekolah_id = $sekolah[$a]->id;
      //     $kelas[$x]->nama = "KLS " . $x;
      //     $kelas[$x]->jurusan = "Jurusan ";
      //     $kelas[$x]->save();
      //     $Registrasi[$x] = new \App\Registrasi;
      //     $Registrasi[$x]->kelas_id = $kelas[$x]->id;
      //     $Registrasi[$x]->sekolah_id = $sekolah[$a]->id;
      //     $Registrasi[$x]->tgl = date("Y-m-d");
      //     $Registrasi[$x]->kas_mingguan = 5000;
      //     $Registrasi[$x]->status = 'Actived';
      //     $Registrasi[$x]->save();
      //     $kas[$x] = new \App\Kas;
      //     $kas[$x]->kelas_id = $kelas[$x]->id;
      //     $kas[$x]->kas_bayar = 0;
      //     $kas[$x]->kas_rutin = 0;
      //     $kas[$x]->total_kas = 0;
      //     $kas[$x]->kas_keluar = 0;
      //     $kas[$x]->sisa_kas = 0;
      //     $kas[$x]->save();
      //     for ($i=1 ;$i <2 ; $i++) {
      //       $data[$i] = new \App\User;
      //       $data[$i]->kelas_id = $kelas[$x]->id;
      //       $data[$i]->sekolah_id  = $sekolah[$a]->id;
      //       $data[$i]->nama = "Siswa " . $i;
      //       $data[$i]->password = bcrypt("rahasia");
      //       $data[$i]->save();
      //       $data[$i]->email = "siswa".$data[$i]->id."@gmail.com";
      //       $data[$i]->save();
      //       $memberRole = Role::where('name', 'siswa')->first();
      //       $data[$i]->attachRole($memberRole);
      //       $pemasukan[$i] = new \App\Pemasukan;
      //       $pemasukan[$i]->user_id = $data[$i]->id;
      //       $pemasukan[$i]->user_bayar = 0;
      //       $pemasukan[$i]->save();
      //     }
      //     $bendahara = new \App\User;
      //     $bendahara->kelas_id = $kelas[$x]->id;
      //     $bendahara->sekolah_id  = $sekolah[$a]->id;
      //     $bendahara->nama = "Bendahara " . $kelas[$x]->nama;
      //     $bendahara->password = bcrypt("rahasia");;
      //     $bendahara->save();
      //     $bendahara->email = "bendahara".$bendahara->id."@gmail.com";
      //     $bendahara->save();
      //     $bendaharaRole = Role::where('name', 'petugas')->first();
      //     $bendahara->attachRole($bendaharaRole);
      //     $guru = new \App\User;
      //     $guru->kelas_id = $kelas[$x]->id;
      //     $guru->sekolah_id  = $sekolah[$a]->id;
      //     $guru->nama = "Guru " . $kelas[$x]->nama;
      //     $guru->password = bcrypt("rahasia");;
      //     $guru->save();
      //     $guru->email = "guru".$guru->id."@gmail.com";
      //     $guru->save();
      //     $guruRole = Role::where('name', 'guru')->first();
      //     $guru->attachRole($guruRole);
      //   }
      // }
      // for ($a=1 ;$a <16 ; $a++) {
      //   $sekolah[$a] = new \App\Sekolah;
      //   $sekolah[$a]->nama = "SMA Negri " . $a;
      //   $sekolah[$a]->email = "sman".$a."@gmail.com";
      //   $sekolah[$a]->alamat = "Semarang";
      //   $sekolah[$a]->save();
      //   for ($x=1; $x < 2; $x++) {
      //     $kelas[$x] = new Kelas();
      //     $kelas[$x]->tahun_id = 2;
      //     $kelas[$x]->sekolah_id = $sekolah[$a]->id;
      //     $kelas[$x]->nama = "KLS " . $x;
      //     $kelas[$x]->jurusan = "Jurusan ";
      //     $kelas[$x]->save();
      //     $Registrasi[$x] = new \App\Registrasi;
      //     $Registrasi[$x]->kelas_id = $kelas[$x]->id;
      //     $Registrasi[$x]->sekolah_id = $sekolah[$a]->id;
      //     $Registrasi[$x]->tgl = date("Y-m-d");
      //     $Registrasi[$x]->kas_mingguan = 5000;
      //     $Registrasi[$x]->status = 'Actived';
      //     $Registrasi[$x]->save();
      //     $kas[$x] = new \App\Kas;
      //     $kas[$x]->kelas_id = $kelas[$x]->id;
      //     $kas[$x]->kas_bayar = 0;
      //     $kas[$x]->kas_rutin = 0;
      //     $kas[$x]->total_kas = 0;
      //     $kas[$x]->kas_keluar = 0;
      //     $kas[$x]->sisa_kas = 0;
      //     $kas[$x]->save();
      //     for ($i=1 ;$i <2 ; $i++) {
      //       $data[$i] = new \App\User;
      //       $data[$i]->kelas_id = $kelas[$x]->id;
      //       $data[$i]->sekolah_id  = $sekolah[$a]->id;
      //       $data[$i]->nama = "Siswa " . $i;
      //       $data[$i]->password = bcrypt("rahasia");;
      //       $data[$i]->save();
      //       $data[$i]->email = "siswa".$data[$i]->id."@gmail.com";
      //       $data[$i]->save();
      //       $memberRole = Role::where('name', 'siswa')->first();
      //       $data[$i]->attachRole($memberRole);
      //       $pemasukan[$i] = new \App\Pemasukan;
      //       $pemasukan[$i]->user_id = $data[$i]->id;
      //       $pemasukan[$i]->user_bayar = 0;
      //       $pemasukan[$i]->save();
      //     }
      //     $bendahara = new \App\User;
      //     $bendahara->kelas_id = $kelas[$x]->id;
      //     $bendahara->sekolah_id  = $sekolah[$a]->id;
      //     $bendahara->nama = "Bendahara " . $kelas[$x]->nama;
      //     $bendahara->password = bcrypt("rahasia");;
      //     $bendahara->save();
      //     $bendahara->email = "bendahara".$bendahara->id."@gmail.com";
      //     $bendahara->save();
      //     $guru = new \App\User;
      //     $guru->kelas_id = $kelas[$x]->id;
      //     $guru->sekolah_id  = $sekolah[$a]->id;
      //     $guru->nama = "Guru " . $kelas[$x]->nama;
      //     $guru->password = bcrypt("rahasia");;
      //     $guru->save();
      //     $guru->email = "guru".$guru->id."@gmail.com";
      //     $guru->save();
      //   }
      // }
      // for ($i=1; $i < 12; $i++) {
      //   $sekolah[$i] = new Sekolah();
      //   $sekolah[$i]->nama = "SMK Negeri " . $i;
      //   $sekolah[$i]->alamat "Semarang";
      //   $sekolah[$i]->email = "smk".$i."@sch.id";
      //   $sekolah[$i]->save();
      //   // for ($a=1; $a < 20; $a++) {
      //   //   $kelas[$a] = new Kelas();
      //   //   $kelas[$a]->tahun_id = 2;
      //   //   $kelas[$a]->sekolah_id = $i;
      //   //   $kelas[$a]->nama = "KLS " . $a;
      //   //   $kelas[$a]->jurusan = "Jurusan ".$a;
      //   //   $kelas[$a]->save();
      //   // }
      // }
      // for ($i=1; $i < 12; $i++) {
      //   $sekolah[$i] = new Sekolah();
      //   $sekolah[$i]->nama = "SMA Negeri " . $i;
      //   $sekolah[$i]->alamat "Semarang";
      //   $sekolah[$i]->email = "sma".$i."@sch.id";
      //   $sekolah[$i]->save();
      //   // for ($a=1; $a < 20; $a++) {
      //   //   $kelas[$a] = new Kelas();
      //   //   $kelas[$a]->tahun_id = 2;
      //   //   $kelas[$a]->sekolah_id = $i;
      //   //   $kelas[$a]->nama = "KLS " . $a;
      //   //   $kelas[$a]->jurusan = "Jurusan ".$a;
      //   //   $kelas[$a]->save();
      //   // }
      // }


      // // Membuat Kelas
      // $data->tahun_id = 2;
      // $data = new Kelas();
      // $data->sekolah_id = 2;
      // $data->nama = "XII PS 1";
      // $data->jurusan = "Perawatan Sosial";
      // $data->save();
      // $data = new Kelas();
      // $data->tahun_id = 2;
      // $data->sekolah_id = 2;
      // $data->nama = "XII PS 2";
      // $data->jurusan = "Perawatan Sosial";
      // $data->save();
      // $data = new Kelas();
      // $data->tahun_id = 2;
      // $data->sekolah_id = 2;
      // $data->nama = "XII PS 3";
      // $data->jurusan = "Perawatan Sosial";
      // $data->save();
      // $data = new Kelas();
      // $data->tahun_id = 2;
      // $data->sekolah_id = 2;
      // $data->nama = "XII RPL 1";
      // $data->jurusan = "Rekayasa Perangkat Lunak";
      // $data->save();
      // $data = new Kelas();
      // $data->tahun_id = 2;
      // $data->sekolah_id = 2;
      // $data->nama = "XII RPL 2";
      // $data->jurusan = "Rekayasa Perangkat Lunak";
      // $data->save();
      // $data = new Kelas();
      // $data->tahun_id = 2;
      // $data->sekolah_id = 2;
      // $data->nama = "XII RPL 3";
      // $data->jurusan = "Rekayasa Perangkat Lunak";
      // $data->save();

    }
}
