<?php

use Illuminate\Database\Seeder;

class RegisteredSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data = \App\User::select('users.id as id')->orderBy('id', 'asc')->get();
      $pemasukan = \App\Pemasukan::select('pemasukans.user_id as user_id')->get();

        for ($i=0; $i < count($data); $i++) {
          $pemasukan = new \App\Pemasukan;
          $pemasukan->user_id = $data[$i]->id;
          $pemasukan->user_bayar = 0;
          $pemasukan->save();
        }

        // $user = \App\User::select('users.id as id')->orderBy('id', 'asc')->get();
        //   for ($i=0; $i < count($user); $i++) {
        //     $spp = new \App\Spp;
        //     $spp->user_id = $data[$i]->id;
        //     $spp->user_bayar = 0;
        //     $spp->spp_bayar = 0;
        //     $spp->save();
        //   }
        // $user = \App\User::select('users.id as id')->orderBy('id', 'asc')->get();
        //   for ($i=0; $i < count($user); $i++) {
        //     $absensi = new \App\Absensi;
        //     $absensi->user_id = $data[$i]->id;
        //     $absensi->H = 0;
        //     $absensi->I = 0;
        //     $absensi->S = 0;
        //     $absensi->A = 0;
        //     $absensi->save();
        //   }

        $data1 = \App\Kelas::select('kelas.id as id')->orderBy('id', 'asc')->get();
        // $data2 = \App\Sekolah::join('kelas', 'sekolahs.id','kelas.sekolah_id');

          for ($i=0; $i < count($data1); $i++) {
            $Registrasi = new \App\Registrasi;
            $Registrasi->kelas_id = $data1[$i]->id;
            $Registrasi->sekolah_id = 2;
            $Registrasi->tgl = date("Y-m-d");
            $Registrasi->kas_mingguan = 10000;
            $Registrasi->status = 'Actived';
            $Registrasi->save();
          }

      $data1 = \App\Kelas::select('kelas.id as id')->orderBy('id', 'asc')->get();
      $data2 = \App\Kas::select('kas.kelas_id as kelas_id')->get();

      for ($i=0; $i < count($data1); $i++) {
        $kas = new \App\Kas;\
        $kas->kelas_id = $data1[$i]->id;
        $kas->kas_bayar = 0;
        $kas->kas_rutin = 0;
        $kas->total_kas = 0;
        $kas->kas_keluar = 0;
        $kas->sisa_kas = 0;
        $kas->save();
      }
    }
}
