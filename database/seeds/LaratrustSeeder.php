<?php
use Illuminate\Database\Seeder;
use App\Role;
use App\User;
use App\Kelas;
use Illuminate\Support\Facades\Crypt;
class LaratrustSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
      // Membuat role admin
      $ownerRole = new Role();
      $ownerRole->name = "owner";
      $ownerRole->display_name = "Dawam Raja";
      $ownerRole->description = "";
      $ownerRole->save();

      // Membuat role Bendahara
      $petugasRole = new Role();
      $petugasRole->name = "petugas";
      $petugasRole->display_name = "Bendahara Kelas";
      $petugasRole->description = "";
      $petugasRole->save();

      // Membuat role Bendahara
      // $sekertarisRole = new Role();
      // $sekertarisRole->name = "sekertaris";
      // $sekertarisRole->display_name = "Sekertaris Kelas";
      // $sekertarisRole->description = "";
      // $sekertarisRole->save();

      // Membuat role Siswa
      $siswaRole = new Role();
      $siswaRole->name = "siswa";
      $siswaRole->display_name = "Siswa";
      $siswaRole->description = "";
      $siswaRole->save();

      // Membuat role Guru
      $guruRole = new Role();
      $guruRole->name = "guru";
      $guruRole->display_name = "Guru";
      $guruRole->description = "";
      $guruRole->save();

      // Membuat role Pengelola
      // $adminRole = new Role();
      // $adminRole->name = "admin";
      // $adminRole->display_name = "Pengelola Sekolah";
      // $adminRole->description = "";
      // $adminRole->save();
      //
      // $ortuRole = new Role();
      // $ortuRole->name = "Orangtua";
      // $ortuRole->display_name = "Orang Tua";
      // $ortuRole->description = "";
      // $ortuRole->save();

      // Membuat sample admin
      $owner = new User();
      $owner->nis = 7894;
      $owner->kelas_id = 5;
      $owner->sekolah_id = 2;
      $owner->nama = 'Admin Dawam';
      $owner->email = 'dawam@dev.io';
      $owner->password = bcrypt('rahasia');
      $owner->save();
      $owner->attachRole($ownerRole);

      // Membuat sample Bendahara
      $petugas = new User();
      $petugas->nis = 1111;
      $petugas->kelas_id = 1;
      $petugas->sekolah_id = 2;
      $petugas->nama = "Fafa Rizki Ayu";
      $petugas->email = 'fafa@gmail.com';
      $petugas->password = bcrypt('rahasia');
      $petugas->save();
      $petugas->attachRole($petugasRole);
      $petugas = new User();
      $petugas->nis = 1111+(36*1);
      $petugas->sekolah_id = 2;
      $petugas->kelas_id = 2;
      $petugas->nama = "Fatimah Azzahra";
      $petugas->email = 'fatimah@gmail.com';
      $petugas->password = bcrypt('rahasia');
      $petugas->save();
      $petugas->attachRole($petugasRole);
      $petugas = new User();
      $petugas->nis = 1111+(36*2);
      $petugas->sekolah_id = 2;
      $petugas->kelas_id = 3;
      $petugas->nama = "Natasha";
      $petugas->email = 'natsh@gmail.com';
      $petugas->password = bcrypt('rahasia');
      $petugas->save();
      $petugas->attachRole($petugasRole);
      $petugas = new User();
      $petugas->nis = 1111+(36*3);
      $petugas->kelas_id = 4;
      $petugas->sekolah_id = 2;
      $petugas->nama = "Winda Tri P";
      $petugas->email = 'winda@gmail.com';
      $petugas->password = bcrypt('rahasia');
      $petugas->save();
      $petugas->attachRole($petugasRole);
      $petugas = new User();
      $petugas->nis = 1111+(36*4);
      $petugas->kelas_id = 5;
      $petugas->sekolah_id = 2;
      $petugas->nama = "Risma Nabila";
      $petugas->email = 'risma@gmail.com';
      $petugas->password = bcrypt('rahasia');
      $petugas->save();
      $petugas->attachRole($petugasRole);
      $petugas = new User();
      $petugas->nis = 1111+(36*5);
      $petugas->kelas_id = 6;
      $petugas->sekolah_id = 2;
      $petugas->nama = "Nabila Rizani";
      $petugas->email = 'nabila@gmail.com';
      $petugas->password = bcrypt('rahasia');
      $petugas->save();
      $petugas->attachRole($petugasRole);

      // Membuat sample Sekertaris
      // $sekertaris = new User();
      // $sekertaris->kelas_id = 1;
      // $sekertaris->sekolah_id = 2;
      // $sekertaris->nama = "Umi Purwaningsih";
      // $sekertaris->email = 'um.umik@gmail.com';
      // $sekertaris->password = bcrypt('rahasia');
      // $sekertaris->save();
      // $sekertaris->attachRole($sekertarisRole);
      // $sekertaris = new User();
      // $sekertaris->kelas_id = 2;
      // $sekertaris->sekolah_id = 2;
      // $sekertaris->nama = "Aini";
      // $sekertaris->email = 'aini@gmail.com';
      // $sekertaris->password = bcrypt('rahasia');
      // $sekertaris->save();
      // $sekertaris->attachRole($sekertarisRole);
      // $sekertaris = new User();
      // $sekertaris->kelas_id = 3;
      // $sekertaris->sekolah_id = 2;
      // $sekertaris->nama = "Tika";
      // $sekertaris->email = 'tiktika@gmail.com';
      // $sekertaris->password = bcrypt('rahasia');
      // $sekertaris->save();
      // $sekertaris->attachRole($sekertarisRole);
      // $sekertaris = new User();
      // $sekertaris->kelas_id = 4;
      // $sekertaris->sekolah_id = 2;
      // $sekertaris->nama = "RPL One";
      // $sekertaris->email = 'rpl.one@gmail.com';
      // $sekertaris->password = bcrypt('rahasia');
      // $sekertaris->save();
      // $sekertaris->attachRole($sekertarisRole);
      // $sekertaris = new User();
      // $sekertaris->kelas_id = 5;
      // $sekertaris->sekolah_id = 2;
      // $sekertaris->nama = "Meita";
      // $sekertaris->email = 'meita@gmail.com';
      // $sekertaris->password = bcrypt('rahasia');
      // $sekertaris->save();
      // $sekertaris->attachRole($sekertarisRole);
      // $sekertaris = new User();
      // $sekertaris->kelas_id = 6;
      // $sekertaris->sekolah_id = 2;
      // $sekertaris->nama = "rpl Tiga";
      // $sekertaris->email = 'rpl.tiga@gmail.com';
      // $sekertaris->password = bcrypt('rahasia');
      // $sekertaris->save();
      // $sekertaris->attachRole($sekertarisRole);

      // Membuat sample Siswa
      // $siswa = new User();
      // $siswa->nis = 1112;
      // $siswa->kelas_id = 1;
      // $siswa->sekolah_id = 2;
      // $siswa->nama = "Siswa 1";
      // $siswa->email = 'siswa1@gmail.com';
      // $siswa->password = bcrypt('rahasia');
      // $siswa->save();
      // $siswa->attachRole($siswaRole);
      // $siswa = new User();
      // $siswa->nis = 1112+(34*1);
      // $siswa->kelas_id = 2;
      // $siswa->sekolah_id = 2;
      // $siswa->nama = "Siswa 2";
      // $siswa->email = 'siswa2@gmail.com';
      // $siswa->password = bcrypt('rahasia');
      // $siswa->save();
      // $siswa->attachRole($siswaRole);
      // $siswa = new User();
      // $siswa->nis = 1112+(34*2);
      // $siswa->kelas_id = 3;
      // $siswa->sekolah_id = 2;
      // $siswa->nama = "Siswa 3";
      // $siswa->email = 'siswa3@gmail.com';
      // $siswa->password = bcrypt('rahasia');
      // $siswa->save();
      // $siswa->attachRole($siswaRole);
      // $siswa = new User();
      // $siswa->nis = 1112+(34*3);
      // $siswa->kelas_id = 4;
      // $siswa->sekolah_id = 2;
      // $siswa->nama = "Siswa 4";
      // $siswa->email = 'siswa4@gmail.com';
      // $siswa->password = bcrypt('rahasia');
      // $siswa->save();
      // $siswa->attachRole($siswaRole);
      // $siswa = new User();
      // $siswa->nis = 1112+(34*4);
      // $siswa->kelas_id = 5;
      // $siswa->sekolah_id = 2;
      // $siswa->nama = "Siswa 5";
      // $siswa->email = 'siswa5@gmail.com';
      // $siswa->password = bcrypt('rahasia');
      // $siswa->save();
      // $siswa->attachRole($siswaRole);
      // $siswa = new User();
      // $siswa->nis = 1112+(34*5);
      // $siswa->kelas_id = 6;
      // $siswa->sekolah_id = 2;
      // $siswa->nama = "Siswa 6";
      // $siswa->email = 'siswa6@gmail.com';
      // $siswa->password = bcrypt('rahasia');
      // $siswa->save();
      // $siswa->attachRole($siswaRole);

        // for ($x=1; $x < 23; $x++) {
        //   for ($i=1; $i < 21 ; $i++) {
        //     for ($a=1 ;$a <37 ; $a++) {
        //       $data[$a] = new \App\User;
        //       $data[$a]->kelas_id = $i;
        //       $data[$a]->sekolah_id = $x;
        //       $data[$a]->nama = "Siswa " . $a;
        //       $data[$a]->email = $i."siswa".$a."@gmail.com";
        //       $data[$a]->password = "rahasia".$a;
        //       $data[$a]->save();
        //     }
        //   }
        // }
       // Membuat sample Guru
       $guru = new User();
       $guru->kelas_id = 1;
       $guru->sekolah_id = 2;
       $guru->nama = "Guru 1";
       $guru->email = 'Guru1@gmail.com';
       $guru->password = bcrypt('rahasia');
       $guru->save();
       $guru->attachRole($guruRole);
       $guru = new User();
       $guru->kelas_id = 2;
       $guru->sekolah_id = 2;
       $guru->nama = "Guru 2";
       $guru->email = 'Guru2@gmail.com';
       $guru->password = bcrypt('rahasia');
       $guru->save();
       $guru->attachRole($guruRole);
       $guru = new User();
       $guru->kelas_id = 3;
       $guru->sekolah_id = 2;
       $guru->nama = "Guru 3";
       $guru->email = 'Guru3@gmail.com';
       $guru->password = bcrypt('rahasia');
       $guru->save();
       $guru->attachRole($guruRole);
       $guru = new User();
       $guru->kelas_id = 4;
       $guru->sekolah_id = 2;
       $guru->nama = "Guru 4";
       $guru->email = 'Guru4@gmail.com';
       $guru->password = bcrypt('rahasia');
       $guru->save();
       $guru->attachRole($guruRole);
       $guru = new User();
       $guru->kelas_id = 5;
       $guru->sekolah_id = 2;
       $guru->nama = "Guru 5";
       $guru->email = 'Guru5@gmail.com';
       $guru->password = bcrypt('rahasia');
       $guru->save();
       $guru->attachRole($guruRole);
       $guru = new User();
       $guru->kelas_id = 6;
       $guru->sekolah_id = 2;
       $guru->nama = "Guru 6";
       $guru->email = 'Guru6@gmail.com';
       $guru->password = bcrypt('rahasia');
       $guru->save();
       $guru->attachRole($guruRole);

       //Membuat Sample Pengelola
       // $admin = new User();
       // $admin->sekolah_id = 2;
       // $admin->nama = "Pengelola 1";
       // $admin->email = 'pengelola1@gmail.com';
       // $admin->password = bcrypt('rahasia');
       // $admin->save();
       // $admin->attachRole($adminRole);
       // $admin = new User();
       // $admin->sekolah_id = 2;
       // $admin->nama = "Pengelola 2";
       // $admin->email = 'pengelola2@gmail.com';
       // $admin->password = bcrypt('rahasia');
       // $admin->save();
       // $admin->attachRole($adminRole);
       //
       // $ortu = new User();
       // $ortu->sekolah_id = 2;
       // $ortu->kelas_id = 1;
       // $ortu->nama = "Ortu Umi";
       // $ortu->email = 'ortu.umik@gmail.com';
       // $ortu->password = bcrypt('rahasia');
       // $ortu->save();
       // $ortu->attachRole($ortuRole);
    }

    /**
     * Truncates all the laratrust tables and the users table
     *
     * @return    void
     */

}
