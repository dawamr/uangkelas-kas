<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKasKelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kas_kelas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kelas_id')->unsigned();
            $table->foreign('kelas_id')->references('id')->on('classes')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('kas_masuk');
            $table->integer('kas_keluar');
            $table->integer('total_kas');
            $table->integer('sisa_kas');
            $table->integer('kas_rutin');
            $table->integer('batas_pinjam');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kas_kelas');
    }
}
