<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->integer('provinsi_id')->unsigned();
            $table->foreign('provinsi_id')->references('id')->on('provinces')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('kota_id')->unsigned();
            $table->foreign('kota_id')->references('id')->on('cities')->onDelete('cascade')->onUpdate('cascade');
            $table->enum('jenjang',['sd','smp','sma','smk']);
            $table->string('email');
            $table->string('website')->nullable();
            $table->string('alamat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
